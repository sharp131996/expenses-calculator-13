import React, {ReactElement} from 'react';
import firebase from 'firebase';
import {useForm} from 'react-hook-form';
import {notification} from 'antd';
import {ResetPasswordType} from '../types/resetPassword.type';

const ResetPassword = (): ReactElement => {
  const auth = firebase.auth();
  const { register, reset, handleSubmit } = useForm<ResetPasswordType>();

  const resetPassword = async (form: ResetPasswordType) => {
    try {
      await auth.sendPasswordResetEmail(form.email);

      reset({email: ''});
      notification.success({message: 'Password is reset'});
    } catch (err) {
      notification.error({ message: err.message });
    }
  };

  return (
    <form onSubmit={handleSubmit(resetPassword)}>
      <input
        type="email"
        {...register('email', { required: true })}
        placeholder="Email"
      />

      <button type="submit">Reset password</button>
    </form>
  );
};

export default ResetPassword;
