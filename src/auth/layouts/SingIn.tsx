import React, {ReactElement} from 'react';
import firebase from 'firebase';
import {useForm} from 'react-hook-form';
import {AuthorizeType} from '../types/authorize.type';
import {notification} from 'antd';
import {RouteEnum} from '../../shared/enums/route.enum';
import {useNavigate} from "react-router-dom";

const SignIn = (): ReactElement => {
  const auth = firebase.auth();
  const { register, reset, handleSubmit } = useForm<AuthorizeType>();
  const navigate = useNavigate();

  const signIn = async (form: AuthorizeType) => {
    try {
      await auth.signInWithEmailAndPassword(form.email, form.password);

      reset({email: '', password: ''});
      navigate(`/${RouteEnum.expenses}`);
      notification.success({message: 'You are signed in'});
    } catch (err) {
      notification.error({ message: err.message });
    }
  };

  return (
    <form onSubmit={handleSubmit(signIn)}>
      <input
        type="email"
        {...register('email', { required: true })}
        placeholder="Email"
      />
      <input
        type="password"
        {...register('password', { required: true })}
        placeholder="Password"
      />

      <button>Sing in</button>
    </form>
  );
};

export default SignIn;

