import React, {ReactElement} from 'react';
import firebase from 'firebase';
import {useForm} from 'react-hook-form';
import {AuthorizeType} from '../types/authorize.type';
import {notification} from 'antd';
import {useNavigate} from 'react-router-dom';
import {RouteEnum} from '../../shared/enums/route.enum';

const SignUp = (): ReactElement => {
  const auth = firebase.auth();
  const { register, reset, handleSubmit } = useForm<AuthorizeType>();
  const navigate = useNavigate();

  const signUp = async (form: AuthorizeType) => {
    try {
      await auth.createUserWithEmailAndPassword(form.email, form.password);

      reset({email: '', password: ''});
      navigate(`/${RouteEnum.expenses}`);
      notification.success({message: 'You are signed up'});
    } catch (err) {
      notification.error({ message: err.message });
    }
  };

  return (
    <form onSubmit={handleSubmit(signUp)}>
      <input
        type="email"
        {...register('email', { required: true })}
        placeholder="Email"
      />
      <input
        type="password"
        {...register('password', { required: true })}
        placeholder="Password"
      />

      <button>Sing in</button>
    </form>
  );
};

export default SignUp;
