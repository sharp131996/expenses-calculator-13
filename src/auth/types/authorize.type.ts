export type AuthorizeType = {
  email: string;
  password: string;
}
