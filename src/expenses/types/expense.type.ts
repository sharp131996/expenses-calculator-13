export type ExpenseType = {
  key?: string;
  amount: number;
  categoryKey: string;
  typeOfSavingKey: string;
  createdAt: number | string;
  isPlanned: boolean;
  comment: string;
}
