import {ExpenseType} from "../types/expense.type";
import firebase from "firebase";
import moment from "moment";
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {transformFirebaseList} from '../../shared/helpers/transformFirebaseList';
import {TypeOfSavingCollectionType} from '../../type-of-saving/types/typeOfSavingCollection.type';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from "mathjs";
import {PRECISION} from '../../shared/consts/precision.const';

export const getExpensesFirestore = async (date: string, isShowAll = false, userKey: string): Promise<ExpenseType[]> => {
  const db = firebase.firestore();
  const startDate = isShowAll ?
    moment().subtract(50, 'y').startOf('month') :
    moment(date).startOf('month');
  const endDate = isShowAll ?
    moment().add(50, 'y').endOf('month') :
    moment(date).endOf('month');

  const expensesCollection = await db.collection(EntityPathEnum.expenses)
    .doc(userKey)
    .collection(EntityPathEnum.userExpenses)
    .where('createdAt', '>', Number(startDate))
    .where('createdAt', '<', Number(endDate))
    .orderBy('createdAt', 'desc')
    .get();

  return transformFirebaseList<ExpenseType>(expensesCollection);
}

export const trackEditExpenseFirestore = async (savedExpense: ExpenseType, expenseFormValue: ExpenseType, typesOfSaving: TypeOfSavingCollectionType) => {
  const savedTypeOfSavingOldAmount = typesOfSaving[savedExpense.typeOfSavingKey]?.amount;
  const formTypeOfSavingOldAmount = typesOfSaving[expenseFormValue.typeOfSavingKey]?.amount;
  const isTypeOfSavingChanged = savedExpense.typeOfSavingKey !== expenseFormValue.typeOfSavingKey;

  if (isTypeOfSavingChanged) {
    const expenseFormAmount = chain(savedTypeOfSavingOldAmount).add(expenseFormValue.amount).format(PRECISION).done();
    const savedExpenseFormAmount = chain(savedTypeOfSavingOldAmount).add(savedExpense.amount).format(PRECISION).done();
    const savedTypeOfSavingAmount = expenseFormValue.amount === savedExpense.amount ?
      expenseFormAmount :
      savedExpenseFormAmount;
    const formTypeOfSavingOldAmount = typesOfSaving[expenseFormValue.typeOfSavingKey]?.amount;
    const formTypeOfSavingAmount = chain(formTypeOfSavingOldAmount).add(expenseFormValue.amount).format(PRECISION).done();

    await setTypeOfSavingAmount(savedExpense.typeOfSavingKey, Number(savedTypeOfSavingAmount))
      .catch((err) => console.log(err));
    await setTypeOfSavingAmount(expenseFormValue.typeOfSavingKey, Number(formTypeOfSavingAmount))
      .catch((err) => console.log(err));

    return;
  }

  const isAmountChanged = expenseFormValue.amount !== savedExpense.amount && formTypeOfSavingOldAmount;

  if (isAmountChanged) {
    const changedAmount = chain(expenseFormValue.amount).subtract(savedExpense.amount).format(PRECISION).done();
    const formTypeOfSavingAmount = chain(formTypeOfSavingOldAmount).subtract(changedAmount).format(PRECISION).done();

    await setTypeOfSavingAmount(savedExpense.typeOfSavingKey, Number(formTypeOfSavingAmount))
      .catch((err) => console.log(err));
  }
};
