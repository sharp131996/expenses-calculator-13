import React, {ReactElement, useContext, useState} from 'react';
import {ExpenseType} from "../types/expense.type";
import DeleteExpense from "./DeleteExpense";
import moment from "moment";
import EditExpense from "./EditExpense";
import {ExpensesCategoryCollectionType} from '../../expense-categories/types/expensesCategoryCollection.type';
import {TABLE_DATE_FORMAT} from '../../shared/consts/dateFormat';
import {CurrencyExchangeContext, ExpensesContext, TypesOfSavingContext, UserContext, UserInfoContext} from '../../App';
import {CURRENCY_NAME} from '../../shared/consts/currencyName';
import {calculateAmountInBaseCurrency} from '../../shared/helpers/calculateAmountInBaseCurrency';
import {Typography} from 'antd';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import firebase from 'firebase';
import {useMounted} from '../../shared/hooks/useMounted';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';

type PlannedExpensesListProps = {
  expenseCategories: ExpensesCategoryCollectionType;
  expenses: ExpenseType[];
}

const PlannedExpensesTable = (
  {
    expenseCategories,
    expenses,
  }: PlannedExpensesListProps
): ReactElement => {
  const db = firebase.firestore();

  const { user } = useContext(UserContext);
  const { getExpenses } = useContext(ExpensesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const { rates } = useContext(CurrencyExchangeContext);

  const [editExpense, setEditExpense] = useState<ExpenseType | null>();
  const isMounted = useMounted();

  const resetEditExpense = (): void => {
    setEditExpense(null);
  };

  const setPlannedExpenseDone = async (expense: ExpenseType | null): Promise<void> => {
    if (!expense) { return; }

    expense.isPlanned = false;
    const typeOfSavingOldAmount = typesOfSaving[expense.typeOfSavingKey]?.amount;
    const typeOfSavingAmount = chain(typeOfSavingOldAmount).subtract(expense.amount).format(PRECISION).done();

    await db.collection(EntityPathEnum.expenses)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenses)
      .doc(expense.key)
      .update(expense);

    if (typeOfSavingAmount) {
      await setTypeOfSavingAmount(expense.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      await getExpenses(user.key);
      await getTypesOfSaving(user.key);
    }
  };

  const expensesList = expenses.map((expense: ExpenseType, index: number) => {
    const expenseId = expense.key;
    const formattedPlannedDate = moment(expense.createdAt).format(TABLE_DATE_FORMAT);
    const isPassedPlannedDate = moment(expense.createdAt).isBefore(moment());
    const wrappedFormattedDate = isPassedPlannedDate ?
      <Typography.Text type="danger">{formattedPlannedDate}</Typography.Text> :
      formattedPlannedDate;
    const category = expenseCategories[expense.categoryKey];
    const typeOfSaving = typesOfSaving[expense.typeOfSavingKey];
    const typeOfSavingCurrency = CURRENCY_NAME[typeOfSaving?.currencyKey];
    const rate = rates[typeOfSaving?.currencyKey]?.saleRate || 1;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, expense.amount, rate, typeOfSaving);

    return (
      <tr key={expenseId}>
        <td>{index + 1}</td>
        <td>{category?.name || 'unknown'}</td>
        <td>{expense.amount}</td>
        <td>{amountInBaseCurrency}</td>
        <td>{typeOfSaving?.name || 'deleted'} {typeOfSavingCurrency}</td>
        <td>{expense.comment}</td>
        <td>{wrappedFormattedDate}</td>
        <td>
          <button onClick={() => setPlannedExpenseDone(expense)}>Is done?</button>
          <button onClick={() => setEditExpense(expense)}>Edit</button>
          {
            expense.key &&
              <DeleteExpense
                expense={expense}
              />
          }
        </td>
      </tr>
    );
  });

  return (
    <>
      {
        editExpense ?
          <EditExpense
            expense={editExpense}
            resetEditExpense={resetEditExpense}
          /> :
          null
      }

      {expenses.length === 0 ?
        "No planned expenses" :
        <>
          <h1>Planned table</h1>

          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Category</th>
              <th>Amount</th>
              <th>Amount in base currency</th>
              <th>Type of saving</th>
              <th>Comment</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {expensesList}
            </tbody>
          </table>
        </>
      }
    </>
  );
}

export default PlannedExpensesTable;
