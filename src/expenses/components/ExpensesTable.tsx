import React, {ReactElement, SyntheticEvent, useContext, useEffect, useState} from 'react';
import {ExpenseType} from "../types/expense.type";
import DeleteExpense from "./DeleteExpense";
import moment from "moment";
import EditExpense from "./EditExpense";
import {ExpensesCategoryCollectionType} from '../../expense-categories/types/expensesCategoryCollection.type';
import {MONTH_DATE_FORMAT, TABLE_DATE_FORMAT} from '../../shared/consts/dateFormat';
import {CurrencyExchangeContext, ExpensesContext, TypesOfSavingContext, UserInfoContext} from '../../App';
import {CURRENCY_NAME} from '../../shared/consts/currencyName';
import {calculateAmountInBaseCurrency} from '../../shared/helpers/calculateAmountInBaseCurrency';

type ExpensesListProps = {
  expenseCategories: ExpensesCategoryCollectionType;
  expenses: ExpenseType[];
}

const ExpensesTable = (
  {
    expenseCategories,
    expenses,
  }: ExpensesListProps
): ReactElement => {
  const { setExpensesFilterDate, isShowAllExpenses, setIsShowAllExpenses } = useContext(ExpensesContext);
  const { typesOfSaving } = useContext(TypesOfSavingContext);
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const { rates } = useContext(CurrencyExchangeContext);

  const [editExpense, setEditExpense] = useState<ExpenseType | null>();
  const currentMonth = moment().format(MONTH_DATE_FORMAT);
  const toggleShowAllExpensesButtonText = isShowAllExpenses ? 'Hide' : 'Show';

  useEffect(() => {
    const date = new Date(currentMonth).toISOString();
    setExpensesFilterDate(date);
  }, [setExpensesFilterDate, currentMonth]);

  const onMonthChange = (event: SyntheticEvent) => {
    const month = (event.target as HTMLInputElement).value;
    const date = new Date(month).toISOString();
    setExpensesFilterDate(date);
  };

  const resetEditExpense = (): void => {
    setEditExpense(null);
  };

  const toggleShowAllExpenses = () => {
    setIsShowAllExpenses(!isShowAllExpenses);
  };

  const expensesList = expenses.map((expense: ExpenseType, index: number) => {
    const expenseId = expense.key;
    const formattedDate = moment(expense.createdAt).format(TABLE_DATE_FORMAT);
    const category = expenseCategories[expense.categoryKey];
    const typeOfSaving = typesOfSaving[expense.typeOfSavingKey];
    const typeOfSavingCurrency = CURRENCY_NAME[typeOfSaving?.currencyKey];
    const rate = rates[typeOfSaving?.currencyKey]?.saleRate || 1;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, expense.amount, rate, typeOfSaving);

    return (
      <tr key={expenseId}>
        <td>{index + 1}</td>
        <td>{category?.name || 'unknown'}</td>
        <td>{expense.amount}</td>
        <td>{amountInBaseCurrency}</td>
        <td>{typeOfSaving?.name || 'deleted'} {typeOfSavingCurrency}</td>
        <td>{expense.comment}</td>
        <td>{formattedDate}</td>
        <td>
          <button onClick={() => {
            setEditExpense(expense);
          }}>Edit</button>
          {
            expense.key &&
              <DeleteExpense
                expense={expense}
              />
          }
        </td>
      </tr>
    );
  });

  return (
    <>
      {
        editExpense ?
          <EditExpense
            expense={editExpense}
            resetEditExpense={resetEditExpense}
          /> :
          null
      }

      <div>
        <div>
          <button onClick={toggleShowAllExpenses}>
            {toggleShowAllExpensesButtonText} all expenses
          </button>
        </div>

        <>
          Filter
          <input type="month" defaultValue={currentMonth} onChange={onMonthChange} disabled={isShowAllExpenses}/>
        </>
      </div>

      {expenses.length === 0 ?
        "No expenses" :
        <>
          <h1>Expenses table</h1>

          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Category</th>
              <th>Amount</th>
              <th>Amount in base currency</th>
              <th>Type of saving</th>
              <th>Comment</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {expensesList}
            </tbody>
          </table>
        </>
      }
    </>
  );
}

export default ExpensesTable;
