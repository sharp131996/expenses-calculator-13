import React, {ReactElement, useContext, useState} from 'react';
import {ExpenseType} from "../types/expense.type";
import {useForm} from "react-hook-form";
import firebase from "firebase/app";
import 'firebase/firestore';
import {ExpenseForm} from './ExpenseForm';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpensesContext, TypesOfSavingContext, UserContext} from '../../App';
import moment from 'moment';
import {DATE_INPUT_FORMAT} from '../../shared/consts/dateFormat';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

const CreateExpense = (): ReactElement => {
  const { getExpenses } = useContext(ExpensesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const { register, handleSubmit, reset, setValue, getValues } = useForm<ExpenseType>({
    defaultValues: {
      createdAt: moment().format(DATE_INPUT_FORMAT),
    }
  });
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);

  const onSubmit = async (expense: ExpenseType): Promise<void> => {
    setLoading(true);

    const typeOfSavingOldAmount = typesOfSaving[expense.typeOfSavingKey]?.amount;
    const typeOfSavingAmount = chain(typeOfSavingOldAmount).subtract(expense.amount).format(PRECISION).done();

    expense.createdAt = setDateTime(expense.createdAt);

    await db.collection(EntityPathEnum.expenses)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenses)
      .doc()
      .set(expense);

    setLoading(false);

    if (!expense.isPlanned) {
      await setTypeOfSavingAmount(expense.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      await getExpenses(user.key);
      await getTypesOfSaving(user.key);
    }

    reset();
  }

  return <ExpenseForm
    isCreate
    register={register}
    setValue={setValue}
    getValues={getValues}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default CreateExpense;
