import React, {ReactElement, useContext} from 'react';
import {UseFormGetValues, UseFormRegisterReturn, UseFormSetValue} from 'react-hook-form/dist/types/form';
import {ExpenseType} from '../types/expense.type';
import {ExpenseCategoriesContext} from '../../App';
import TypeOfSavingSelect from '../../shared/components/TypeOfSavingSelect';
import {sortCategoriesByPosition} from '../../shared/helpers/sortCategoriesByPosition';
import {RegisterOptionsType} from '../../type-of-saving/types/registerOptions.type';

type ExpenseFormProps = {
  isCreate?: boolean;
  register(fieldName: string, options?: RegisterOptionsType): UseFormRegisterReturn;
  setValue: UseFormSetValue<ExpenseType>;
  getValues: UseFormGetValues<ExpenseType>;
  onSubmit(): Promise<void>;
  loading: boolean;
}

export const ExpenseForm = (
  {
    isCreate,
    register,
    setValue,
    getValues,
    onSubmit,
    loading,
  }: ExpenseFormProps
): ReactElement => {
  const { expenseCategories } = useContext(ExpenseCategoriesContext);

  const caption = isCreate ? 'Create' : 'Edit';
  const currentDate = isCreate ? new Date().toISOString().substr(0,10) : undefined;
  const sortedExpenseCategoriesKeys = Object.values(expenseCategories)
    .sort(sortCategoriesByPosition)
    .map((category) => category.key);

  const expenseCategoriesOptions = sortedExpenseCategoriesKeys && sortedExpenseCategoriesKeys
    .map((key, index) => {
      if (!index) {
        setValue('categoryKey', key);
      }

      return <option value={key} key={key}>{expenseCategories[key].name}</option>;
    });

  const setTypeOfSavingKeySelect = (typeOfSavingKey: string) => {
    setValue('typeOfSavingKey', typeOfSavingKey);
  };

  return (
    <form onSubmit={onSubmit} autoComplete="off">
      <h1>{caption} expense</h1>
      <select {...register("categoryKey", { required: true })}>
        {expenseCategoriesOptions}
      </select>
      <input
        {...register("amount", { required: true, valueAsNumber: true })}
        type="number"
        placeholder="Amount"
        step="0.01"
        min={1}
      />
      <TypeOfSavingSelect
        {...register('typeOfSavingKey')}
        setValue={isCreate ? setTypeOfSavingKeySelect : undefined}
        typeOfSavingSelectKey={getValues('typeOfSavingKey')}
      />
      <input
        {...register("createdAt", { required: true })}
        type="date"
        defaultValue={currentDate}
      />
      {
        isCreate ?
          (
            <>
              <label htmlFor="isPlanned">Is planned</label>
              <input
                id="isPlanned"
                {...register("isPlanned")}
                type="checkbox"
              />
            </>
          ) :
          null
      }
      <textarea
        {...register("comment")}
        placeholder="Comment"
      />

      <button type="submit" disabled={loading}>{caption}</button>
    </form>
  )
}
