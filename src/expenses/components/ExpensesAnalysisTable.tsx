import React, {ReactElement} from 'react';
import 'firebase/firestore';
import {ExpensesCategoryCollectionType} from '../../expense-categories/types/expensesCategoryCollection.type';
import {ExpenseType} from '../types/expense.type';
import {round} from '../../shared/helpers/round';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

type ExpensesAnalysisTableProps = {
  expenses: ExpenseType[];
  expenseCategories: ExpensesCategoryCollectionType;
}

const ExpensesAnalysisTable = ({expenses, expenseCategories}: ExpensesAnalysisTableProps): ReactElement => {
  const categories = expenses.map((expense) => expense.categoryKey);
  const uniqueCategories = [...new Set(categories)];
  const totalAmount = expenses
    .reduce((previousExpense, expense) =>
      chain(previousExpense).add(expense.amount).format(PRECISION).done(), 0);

  const categoriesAmount: {[key: string]: number} = {};

  expenses.forEach((expense) => {
    const categoryAmount = categoriesAmount[expense.categoryKey];
    if (categoryAmount) {
      categoriesAmount[expense.categoryKey] = chain(categoryAmount).add(expense.amount).format(PRECISION).done();
    }

    if (!categoryAmount) {
      categoriesAmount[expense.categoryKey] = expense.amount;
    }
  });

  const mappedUniqueCategories = uniqueCategories.map(key => {
    const percentage = (categoriesAmount[key] / totalAmount) * 100;
    const roundedPercentage = round(percentage);
    const expenseCategory = expenseCategories[key];

    return {
      key,
      roundedPercentage,
      totalCategoryPrice: categoriesAmount[key],
      categoryName: expenseCategory?.name || 'unknown',
    };
  });

  const sortedMappedUniqueCategories = mappedUniqueCategories
    .sort((a, b) => b.totalCategoryPrice - a.totalCategoryPrice);

  const renderTableBody = sortedMappedUniqueCategories.map(category => {
    return (
      <tr key={category.key}>
        <td>{category.categoryName}</td>
        <td>{category.totalCategoryPrice}</td>
        <td>{category.roundedPercentage}%</td>
      </tr>
    );
  });

  return (
    <>
      <h1>Expenses analysis table</h1>

      <table>
        <thead>
        <tr>
          <td>Category name</td>
          <td>Total category price</td>
          <td>Total category %</td>
        </tr>
        </thead>
        <tbody>
        {renderTableBody}
        </tbody>
      </table>
    </>
  );
}

export default ExpensesAnalysisTable;
