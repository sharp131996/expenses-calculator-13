import React, {ReactElement, useContext, useState} from 'react';
import firebase from "firebase/app";
import 'firebase/firestore';
import {EntityPathEnum} from "../../shared/enums/entityPath.enum";
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpensesContext, TypesOfSavingContext, UserContext} from '../../App';
import {ExpenseType} from '../types/expense.type';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

type DeleteExpenseType = {
  expense: ExpenseType;
}

const DeleteExpense = ({expense}: DeleteExpenseType): ReactElement => {
  const { getExpenses } = useContext(ExpensesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const onDelete = async (): Promise<void> => {
    setLoading(true);

    const typeOfSavingOldAmount = typesOfSaving[expense.typeOfSavingKey]?.amount;

    await db
      .collection(EntityPathEnum.expenses)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenses)
      .doc(expense.key)
      .delete();

    if (typeOfSavingOldAmount && !expense.isPlanned) {
      const typeOfSavingAmount = chain(typeOfSavingOldAmount).add(expense.amount).format(PRECISION).done();
      await setTypeOfSavingAmount(expense.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      getExpenses(user.key);
      getTypesOfSaving(user.key);
    }

    setLoading(false);
  };

  return <button onClick={onDelete} disabled={loading}>Delete</button>;
}

export default DeleteExpense;
