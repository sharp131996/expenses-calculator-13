import React, {ReactElement, useContext, useEffect, useState} from 'react';
import {ExpenseType} from "../types/expense.type";
import {useForm} from "react-hook-form";
import moment from "moment";
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {ExpenseForm} from './ExpenseForm';
import {DATE_INPUT_FORMAT} from '../../shared/consts/dateFormat';
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpensesContext, TypesOfSavingContext, UserContext} from '../../App';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {trackEditExpenseFirestore} from '../effects/expenses';

type EditExpenseProps = {
  expense: ExpenseType;
  resetEditExpense: () => void;
}

const EditExpense = ({expense, resetEditExpense}: EditExpenseProps): ReactElement => {
  const { getExpenses } = useContext(ExpensesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const createdAt = new Date(expense.createdAt).toISOString();

  const [loading, setLoading] = useState(false);
  const isMounted = useMounted();
  const { register, handleSubmit, reset, setValue, getValues } = useForm<ExpenseType>();

  useEffect(() => {
    setValue("amount", expense.amount);
    setValue("categoryKey", expense.categoryKey);
    setValue("typeOfSavingKey", expense.typeOfSavingKey);
    setValue("createdAt", moment(createdAt).format(DATE_INPUT_FORMAT));
    setValue("comment", expense.comment);
    setValue("isPlanned", expense.isPlanned);
  }, [setValue, expense, createdAt]);

  const onSubmit = async (expenseFormValue: ExpenseType): Promise<void> => {
    setLoading(true);

    const formattedDateExpense = {...expenseFormValue};
    formattedDateExpense.createdAt = setDateTime(formattedDateExpense.createdAt, Number(expense.createdAt));
    formattedDateExpense.isPlanned = Boolean(formattedDateExpense.isPlanned);

    await db.collection(EntityPathEnum.expenses)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenses)
      .doc(expense.key)
      .update(formattedDateExpense);

    setLoading(false);

    if (!formattedDateExpense.isPlanned) {
      await trackEditExpenseFirestore(expense, expenseFormValue, typesOfSaving);
    }

    if (isMounted) {
      await getExpenses(user.key);
      await getTypesOfSaving(user.key);
      resetEditExpense();
    }

    reset();
  }

  return <ExpenseForm
    register={register}
    setValue={setValue}
    getValues={getValues}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default EditExpense;
