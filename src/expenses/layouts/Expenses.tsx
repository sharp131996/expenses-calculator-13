import React, {ReactElement, useContext} from 'react';
import ExpensesTable from "../components/ExpensesTable";
import CreateExpense from "../components/CreateExpense";
import ExpensesAnalysisTable from '../components/ExpensesAnalysisTable';
import {ExpenseCategoriesContext, ExpensesContext} from '../../App';
import PlannedExpensesTable from '../components/PlannedExpensesTable';

const Expenses = (): ReactElement => {
  const { expenseCategories } = useContext(ExpenseCategoriesContext);
  const { expenses } = useContext(ExpensesContext);
  const notPlannedExpenses = expenses.filter((expense): boolean => !expense.isPlanned);
  const plannedExpenses = expenses.filter((expense): boolean => Boolean(expense.isPlanned));

  return (
    <>
      <CreateExpense/>
      <ExpensesAnalysisTable
        expenses={notPlannedExpenses}
        expenseCategories={expenseCategories}
      />
      <ExpensesTable
        expenses={notPlannedExpenses}
        expenseCategories={expenseCategories}
      />
      <PlannedExpensesTable
        expenses={plannedExpenses}
        expenseCategories={expenseCategories}
      />
    </>
  );
};

export default Expenses;
