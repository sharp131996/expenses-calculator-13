import React, {ReactElement, useContext} from 'react';
import IncomesTable from "../components/IncomesTable";
import CreateIncome from "../components/CreateIncome";
import {IncomesContext} from '../../App';
import PlannedIncomesTable from '../components/PlannedIncomesTable';

const Incomes = (): ReactElement => {
  const { incomes } = useContext(IncomesContext);
  const notPlannedIncomes = incomes.filter((income) => !income.isPlanned);
  const plannedIncomes = incomes.filter((income) => income.isPlanned);

  return (
    <>
      <CreateIncome/>
      <IncomesTable incomes={notPlannedIncomes}/>
      <PlannedIncomesTable incomes={plannedIncomes}/>
    </>
  );
};

export default Incomes;
