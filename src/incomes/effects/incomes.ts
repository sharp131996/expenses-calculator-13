import {IncomeType} from "../types/income.type";
import firebase from "firebase";
import moment from "moment";
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {transformFirebaseList} from '../../shared/helpers/transformFirebaseList';
import {TypeOfSavingCollectionType} from '../../type-of-saving/types/typeOfSavingCollection.type';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

export const getIncomesFirestore = async (date: string, isShowAllIncomes = false, userKey: string): Promise<IncomeType[]> => {
  const db = firebase.firestore();
  const startDate = isShowAllIncomes ?
    moment().subtract(50, 'y').startOf('month') :
    moment(date).startOf('month');
  const endDate = isShowAllIncomes ?
    moment().add(50, 'y').endOf('month') :
    moment(date).endOf('month');

  const incomesCollection = await db.collection(EntityPathEnum.incomes)
    .doc(userKey)
    .collection(EntityPathEnum.userIncomes)
    .where('createdAt', '>', Number(startDate))
    .where('createdAt', '<', Number(endDate))
    .orderBy('createdAt', 'desc')
    .get();

  return transformFirebaseList<IncomeType>(incomesCollection);
}

export const trackEditIncomeFirestore = async (savedIncome: IncomeType, incomeFormValue: IncomeType, typesOfSaving: TypeOfSavingCollectionType) => {
  const savedTypeOfSavingOldAmount = typesOfSaving[savedIncome.typeOfSavingKey]?.amount;
  const formTypeOfSavingOldAmount = typesOfSaving[incomeFormValue.typeOfSavingKey]?.amount;
  const isTypeOfSavingChanged = savedIncome.typeOfSavingKey !== incomeFormValue.typeOfSavingKey;

  if (isTypeOfSavingChanged) {
    const formIncomeAmount = chain(savedTypeOfSavingOldAmount).subtract(incomeFormValue.amount).format(PRECISION).done();
    const savedIncomeAmount = chain(savedTypeOfSavingOldAmount).subtract(savedIncome.amount).format(PRECISION).done();
    const savedTypeOfSavingAmount = incomeFormValue.amount === savedIncome.amount ?
      formIncomeAmount :
      savedIncomeAmount;
    const formTypeOfSavingOldAmount = typesOfSaving[incomeFormValue.typeOfSavingKey]?.amount;
    const formTypeOfSavingAmount = chain(formTypeOfSavingOldAmount).add(incomeFormValue.amount).format(PRECISION).done();

    await setTypeOfSavingAmount(savedIncome.typeOfSavingKey, Number(savedTypeOfSavingAmount))
      .catch((err) => console.log(err));
    await setTypeOfSavingAmount(incomeFormValue.typeOfSavingKey, Number(formTypeOfSavingAmount))
      .catch((err) => console.log(err));

    return;
  }

  if (incomeFormValue.amount !== savedIncome.amount && formTypeOfSavingOldAmount) {
    const changedAmount = chain(incomeFormValue.amount).subtract(savedIncome.amount).format(PRECISION).done();
    const formTypeOfSavingAmount = chain(formTypeOfSavingOldAmount).add(changedAmount).format(PRECISION).done();

    await setTypeOfSavingAmount(savedIncome.typeOfSavingKey, Number(formTypeOfSavingAmount))
      .catch((err) => console.log(err));
  }
};
