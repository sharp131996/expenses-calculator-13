import React, {ReactElement, useContext, useEffect, useState} from 'react';
import {IncomeType} from "../types/income.type";
import {useForm} from "react-hook-form";
import moment from "moment";
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {IncomeForm} from './IncomeForm';
import {DATE_INPUT_FORMAT} from '../../shared/consts/dateFormat';
import {useMounted} from '../../shared/hooks/useMounted';
import {IncomesContext, TypesOfSavingContext, UserContext} from '../../App';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {trackEditIncomeFirestore} from '../effects/incomes';

type EditIncomeProps = {
  income: IncomeType;
  resetEditIncome: () => void;
}

const EditIncome = ({income, resetEditIncome}: EditIncomeProps): ReactElement => {
  const { getIncomes } = useContext(IncomesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const createdAt = new Date(income.createdAt).toISOString();

  const [loading, setLoading] = useState(false);
  const isMounted = useMounted();
  const { register, handleSubmit, reset, setValue, getValues } = useForm<IncomeType>();

  useEffect(() => {
    setValue("amount", income.amount);
    setValue("typeOfSavingKey", income.typeOfSavingKey);
    setValue("createdAt", moment(createdAt).format(DATE_INPUT_FORMAT));
    setValue("isPlanned", income.isPlanned);
    setValue("comment", income.comment);
  }, [setValue, income, createdAt]);

  const onSubmit = async (incomeFormValue: IncomeType): Promise<void> => {
    setLoading(true);

    const formattedDateIncome = {...incomeFormValue};
    formattedDateIncome.createdAt = setDateTime(formattedDateIncome.createdAt, Number(income.createdAt));
    formattedDateIncome.isPlanned = Boolean(formattedDateIncome.isPlanned);

    await db.collection(EntityPathEnum.incomes)
      .doc(user.key)
      .collection(EntityPathEnum.userIncomes)
      .doc(income.key)
      .update(formattedDateIncome);

    if (!formattedDateIncome.isPlanned) {
      await trackEditIncomeFirestore(income, incomeFormValue, typesOfSaving);
    }

    if (isMounted) {
      await getIncomes(user.key);
      await getTypesOfSaving(user.key);
      resetEditIncome();
    }

    reset();

    setLoading(false);
  }

  return <IncomeForm
    register={register}
    setValue={setValue}
    getValues={getValues}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default EditIncome;
