import React, {ReactElement, useContext, useState} from 'react';
import {IncomeType} from "../types/income.type";
import {useForm} from "react-hook-form";
import firebase from "firebase/app";
import 'firebase/firestore';
import {IncomeForm} from './IncomeForm';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {useMounted} from '../../shared/hooks/useMounted';
import {IncomesContext, TypesOfSavingContext, UserContext} from '../../App';
import moment from 'moment';
import {DATE_INPUT_FORMAT} from '../../shared/consts/dateFormat';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

const CreateIncome = (): ReactElement => {
  const { getIncomes } = useContext(IncomesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const { register, handleSubmit, reset, setValue, getValues } = useForm<IncomeType>({
    defaultValues: {
      createdAt: moment().format(DATE_INPUT_FORMAT),
    }
  });
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);

  const onSubmit = async (income: IncomeType): Promise<void> => {
    setLoading(true);

    const typeOfSavingOldAmount = typesOfSaving[income.typeOfSavingKey]?.amount;
    const typeOfSavingAmount = chain(typeOfSavingOldAmount).add(income.amount).format(PRECISION).done();

    income.createdAt = setDateTime(income.createdAt);

    await db.collection(EntityPathEnum.incomes)
      .doc(user.key)
      .collection(EntityPathEnum.userIncomes)
      .doc()
      .set(income);

    if (!income.isPlanned) {
      await setTypeOfSavingAmount(income.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      await getIncomes(user.key);
      await getTypesOfSaving(user.key);
    }
    reset();

    setLoading(false);
  }

  return <IncomeForm
    isCreate
    register={register}
    setValue={setValue}
    getValues={getValues}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default CreateIncome;
