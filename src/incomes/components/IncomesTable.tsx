import React, {ReactElement, SyntheticEvent, useContext, useEffect, useState} from 'react';
import {IncomeType} from "../types/income.type";
import DeleteIncome from "./DeleteIncome";
import moment from "moment";
import EditIncome from "./EditIncome";
import {MONTH_DATE_FORMAT, TABLE_DATE_FORMAT} from '../../shared/consts/dateFormat';
import {CurrencyExchangeContext, IncomesContext, TypesOfSavingContext, UserInfoContext} from '../../App';
import {CURRENCY_NAME} from '../../shared/consts/currencyName';
import {calculateAmountInBaseCurrency} from '../../shared/helpers/calculateAmountInBaseCurrency';

type IncomesListProps = {
  incomes: IncomeType[];
}

const IncomesTable = (
  {
    incomes,
  }: IncomesListProps
): ReactElement => {
  const { setIncomesFilterDate, isShowAllIncomes, setIsShowAllIncomes } = useContext(IncomesContext);
  const { typesOfSaving } = useContext(TypesOfSavingContext);
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const { rates } = useContext(CurrencyExchangeContext);

  const [editIncome, setEditIncome] = useState<IncomeType | null>();
  const currentMonth = moment().format(MONTH_DATE_FORMAT);
  const toggleShowAllIncomesButtonText = isShowAllIncomes ? 'Hide' : 'Show';

  useEffect(() => {
    const date = new Date(currentMonth).toISOString();
    setIncomesFilterDate(date);
  }, [setIncomesFilterDate, currentMonth]);

  const onMonthChange = (event: SyntheticEvent) => {
    const month = (event.target as HTMLInputElement).value;
    const date = new Date(month).toISOString();
    setIncomesFilterDate(date);
  };

  const resetEditIncome = (): void => {
    setEditIncome(null);
  };

  const toggleShowAllIncomes = () => {
    setIsShowAllIncomes(!isShowAllIncomes);
  };

  const incomesList = incomes.map((income: IncomeType, index: number) => {
    const incomeId = income.key;
    const formattedDate = moment(income.createdAt).format(TABLE_DATE_FORMAT);
    const typeOfSaving = typesOfSaving[income.typeOfSavingKey];
    const typeOfSavingCurrency = CURRENCY_NAME[typeOfSaving?.currencyKey];
    const rate = rates[typeOfSaving?.currencyKey]?.saleRate || 1;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, income.amount, rate, typeOfSaving);

    return (
      <tr key={incomeId}>
        <td>{index + 1}</td>
        <td>{income.amount}</td>
        <td>{amountInBaseCurrency}</td>
        <td>{typeOfSaving?.name || 'deleted'} {typeOfSavingCurrency}</td>
        <td>{income.comment}</td>
        <td>{formattedDate}</td>
        <td>
          <button onClick={() => {
            setEditIncome(income);
          }}>Edit</button>
          {
            income.key &&
              <DeleteIncome
                income={income}
              />
          }
        </td>
      </tr>
    );
  });

  return (
    <>
      {
        editIncome ?
          <EditIncome
            income={editIncome}
            resetEditIncome={resetEditIncome}
          /> :
          null
      }

      <div>
        <div>
          <button onClick={toggleShowAllIncomes}>
            {toggleShowAllIncomesButtonText} all incomes
          </button>
        </div>

        <>
          Filter
          <input type="month" defaultValue={currentMonth} onChange={onMonthChange} disabled={isShowAllIncomes}/>
        </>
      </div>

      {incomes.length === 0 ?
        "No incomes" :
        <>
          <h1>Incomes table</h1>

          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Amount</th>
              <th>Amount in base currency</th>
              <th>Type of saving</th>
              <th>Comment</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {incomesList}
            </tbody>
          </table>
        </>
      }
    </>
  );
}

export default IncomesTable;
