import React, {ReactElement, useContext, useState} from 'react';
import {IncomeType} from "../types/income.type";
import DeleteIncome from "./DeleteIncome";
import moment from "moment";
import EditIncome from "./EditIncome";
import {TABLE_DATE_FORMAT} from '../../shared/consts/dateFormat';
import {
  CurrencyExchangeContext,
  ExpensesContext,
  TypesOfSavingContext,
  UserContext,
  UserInfoContext
} from '../../App';
import {CURRENCY_NAME} from '../../shared/consts/currencyName';
import {calculateAmountInBaseCurrency} from '../../shared/helpers/calculateAmountInBaseCurrency';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import firebase from 'firebase';
import {useMounted} from '../../shared/hooks/useMounted';

type IncomesListProps = {
  incomes: IncomeType[];
}

const PlannedIncomesTable = (
  {
    incomes,
  }: IncomesListProps
): ReactElement => {
  const db = firebase.firestore();

  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const { rates } = useContext(CurrencyExchangeContext);
  const { user } = useContext(UserContext);
  const { getExpenses } = useContext(ExpensesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);

  const [editIncome, setEditIncome] = useState<IncomeType | null>();
  const isMounted = useMounted();

  const setPlannedIncomeDone = async (income: IncomeType | null): Promise<void> => {
    if (!income) { return; }

    income.isPlanned = false;
    const typeOfSavingOldAmount = typesOfSaving[income.typeOfSavingKey]?.amount;
    const typeOfSavingAmount = chain(typeOfSavingOldAmount).add(income.amount).format(PRECISION).done();

    await db.collection(EntityPathEnum.incomes)
      .doc(user.key)
      .collection(EntityPathEnum.userIncomes)
      .doc(income.key)
      .update(income);

    if (typeOfSavingAmount) {
      await setTypeOfSavingAmount(income.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      await getExpenses(user.key);
      await getTypesOfSaving(user.key);
    }
  };

  const resetEditIncome = (): void => {
    setEditIncome(null);
  };

  const incomesList = incomes.map((income: IncomeType, index: number) => {
    const incomeId = income.key;
    const formattedDate = moment(income.createdAt).format(TABLE_DATE_FORMAT);
    const typeOfSaving = typesOfSaving[income.typeOfSavingKey];
    const typeOfSavingCurrency = CURRENCY_NAME[typeOfSaving?.currencyKey];
    const rate = rates[typeOfSaving?.currencyKey]?.saleRate || 1;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, income.amount, rate, typeOfSaving);

    return (
      <tr key={incomeId}>
        <td>{index + 1}</td>
        <td>{income.amount}</td>
        <td>{amountInBaseCurrency}</td>
        <td>{typeOfSaving?.name || 'deleted'} {typeOfSavingCurrency}</td>
        <td>{income.comment}</td>
        <td>{formattedDate}</td>
        <td>
          <button onClick={() => setPlannedIncomeDone(income)}>Is done?</button>
          <button onClick={() => setEditIncome(income)}>Edit</button>
          {
            income.key &&
              <DeleteIncome
                income={income}
              />
          }
        </td>
      </tr>
    );
  });

  return (
    <>
      {
        editIncome ?
          <EditIncome
            income={editIncome}
            resetEditIncome={resetEditIncome}
          /> :
          null
      }

      {incomes.length === 0 ?
        "No planned incomes" :
        <>
          <h1>Planned incomes</h1>

          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Amount</th>
              <th>Amount in base currency</th>
              <th>Type of saving</th>
              <th>Comment</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            {incomesList}
            </tbody>
          </table>
        </>
      }
    </>
  );
}

export default PlannedIncomesTable;
