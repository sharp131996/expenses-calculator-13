import React, {ReactElement} from 'react';
import {UseFormGetValues, UseFormRegisterReturn, UseFormSetValue} from 'react-hook-form/dist/types/form';
import {IncomeType} from '../types/income.type';
import TypeOfSavingSelect from '../../shared/components/TypeOfSavingSelect';
import {RegisterOptionsType} from '../../type-of-saving/types/registerOptions.type';

type IncomeFormProps = {
  isCreate?: boolean;
  register(fieldName: string, options?: RegisterOptionsType): UseFormRegisterReturn;
  setValue: UseFormSetValue<IncomeType>;
  getValues: UseFormGetValues<IncomeType>;
  onSubmit(): Promise<void>;
  loading: boolean;
}

export const IncomeForm = (
  {
    isCreate,
    register,
    setValue,
    getValues,
    onSubmit,
    loading,
  }: IncomeFormProps
): ReactElement => {
  const caption = isCreate ? 'Create' : 'Edit';
  const currentDate = isCreate ? new Date().toISOString().substr(0,10) : undefined;

  const setTypeOfSavingKeySelect = (typeOfSavingKey: string) => {
    setValue('typeOfSavingKey', typeOfSavingKey);
  };

  return (
    <form onSubmit={onSubmit} autoComplete="off">
      <h1>{caption} income</h1>
      <input
        {...register("amount", { required: true, valueAsNumber: true })}
        type="number"
        step="0.01"
        placeholder="Amount"
        min={1}
      />
      <TypeOfSavingSelect
        {...register('typeOfSavingKey')}
        setValue={isCreate ? setTypeOfSavingKeySelect : undefined}
        typeOfSavingSelectKey={getValues('typeOfSavingKey')}
      />
      <input
        {...register("createdAt", { required: true })}
        type="date"
        defaultValue={currentDate}
      />
      {
        isCreate ?
          (
            <>
              <label htmlFor="isPlanned">Is planned</label>
              <input
                id="isPlanned"
                {...register("isPlanned")}
                type="checkbox"
              />
            </>
          ) :
          null
      }
      <textarea
        {...register("comment")}
        placeholder="Comment"
      />

      <button type="submit" disabled={loading}>{caption}</button>
    </form>
  )
}
