import React, {ReactElement, useContext, useState} from 'react';
import firebase from "firebase/app";
import 'firebase/firestore';
import {EntityPathEnum} from "../../shared/enums/entityPath.enum";
import {useMounted} from '../../shared/hooks/useMounted';
import {IncomesContext, TypesOfSavingContext, UserContext} from '../../App';
import {IncomeType} from '../types/income.type';
import {setTypeOfSavingAmount} from '../../shared/helpers/setTypeOfSavingAmount';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

type DeleteIncomeType = {
  income: IncomeType;
}

const DeleteIncome = ({income}: DeleteIncomeType): ReactElement => {
  const { getIncomes } = useContext(IncomesContext);
  const { typesOfSaving, getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const onDelete = async (): Promise<void> => {
    setLoading(true);

    const typeOfSavingOldAmount = typesOfSaving[income.typeOfSavingKey]?.amount;

    await db
      .collection(EntityPathEnum.incomes)
      .doc(user.key)
      .collection(EntityPathEnum.userIncomes)
      .doc(income.key)
      .delete();

    if (typeOfSavingOldAmount && !income.isPlanned) {
      const typeOfSavingAmount = chain(typeOfSavingOldAmount).subtract(income.amount).format(PRECISION).done();

      await setTypeOfSavingAmount(income.typeOfSavingKey, typeOfSavingAmount);
    }

    if (isMounted) {
      await getIncomes(user.key);
      await getTypesOfSaving(user.key);
    }

    setLoading(false);
  };

  return <button onClick={onDelete} disabled={loading}>Delete</button>;
}

export default DeleteIncome;
