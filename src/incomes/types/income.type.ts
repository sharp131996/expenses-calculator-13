export type IncomeType = {
  key?: string;
  amount: number;
  typeOfSavingKey: string;
  createdAt: number | string;
  isPlanned: boolean;
  comment: string;
}
