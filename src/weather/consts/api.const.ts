import {WeatherIntervalType} from '../types/weatherInterval.type';

export const getWeatherApiUrl = (weatherInfo: WeatherIntervalType) => `https://api.openweathermap.org/data/2.5/${weatherInfo}?appid=d3f0b4e55393bc4ff24fd84fa160886f`;
