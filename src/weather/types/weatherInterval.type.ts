import {WeatherInterval} from '../consts/weatherInterval.const';

export type WeatherIntervalType = typeof WeatherInterval[keyof typeof WeatherInterval];
