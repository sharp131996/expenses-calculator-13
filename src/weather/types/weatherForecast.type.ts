import {WeatherTodayType} from './weatherToday.type';

export type WeatherForecastType = Omit<WeatherTodayType, 'city'>;
