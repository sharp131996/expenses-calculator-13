export type WeatherTodayType = {
  city: string;
  cloudiness: number;
  feelsLike: number;
  humidity: number;
  pressure: number;
  temperature: number;
  temperatureMax: number;
  temperatureMin: number;
  windSpeed: number;
  time: string;
}
