export type ApiWeatherItemType = {
  dt_txt: string;
  clouds: { all: number ; };
  main: {
    feels_like: number;
    humidity: number;
    pressure: number;
    temp: number;
    temp_max: number;
    temp_min: number;
  };
  wind: { speed: number; };
};
