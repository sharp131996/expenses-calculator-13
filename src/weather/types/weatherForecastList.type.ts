import {WeatherForecastType} from './weatherForecast.type';
import {WeatherTodayType} from './weatherToday.type';

export type WeatherForecastListType = {
  today: WeatherTodayType;
  forecast: WeatherForecastType[];
}
