export type PositionType = {
  lat: number;
  lon: number;
}
