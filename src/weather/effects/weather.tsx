import {PositionType} from '../types/position.type';
import {getWeatherApiUrl} from '../consts/api.const';
import axios from 'axios';
import {WeatherInterval} from '../consts/weatherInterval.const';
import {WeatherForecastListType} from '../types/weatherForecastList.type';
import {ApiWeatherItemType} from '../types/apiWeatherItem.type';
import {WeatherForecastType} from '../types/weatherForecast.type';

const getPosition = (): Promise<PositionType> => {
  return new Promise<PositionType>((resolve, reject): void => {
    navigator.geolocation.getCurrentPosition((position) => {
      resolve({lon: position.coords.longitude, lat: position.coords.latitude});
    }, reject);
  });
};

export const getWeatherInfo = async (): Promise<WeatherForecastListType> => {
  const position = await getPosition();

  const { data: weatherInfoForecast } = await axios(`${getWeatherApiUrl(WeatherInterval.forecast)}&lat=${position.lat}&lon=${position.lon}&units=metric`);
  const { data: weatherInfo } = await axios(`${getWeatherApiUrl(WeatherInterval.weather)}&lat=${position.lat}&lon=${position.lon}&units=metric`);

  const forecast = weatherInfoForecast.list.map((item: ApiWeatherItemType): WeatherForecastType => {
    return {
      cloudiness: item.clouds.all,
      feelsLike: item.main.feels_like,
      humidity: item.main.humidity,
      pressure: item.main.pressure,
      temperature: item.main.temp,
      temperatureMax: item.main.temp_max,
      temperatureMin: item.main.temp_min,
      windSpeed: item.wind.speed,
      time: item.dt_txt,
    };
  });

  return {
    today: {
      city: weatherInfo.name,
      cloudiness: weatherInfo.clouds.all,
      feelsLike: weatherInfo.main.feels_like,
      humidity: weatherInfo.main.humidity,
      pressure: weatherInfo.main.pressure,
      temperature: weatherInfo.main.temp,
      temperatureMax: weatherInfo.main.temp_max,
      temperatureMin: weatherInfo.main.temp_min,
      windSpeed: weatherInfo.wind.speed,
      time: weatherInfo.dt,
    },
    forecast,
  };
}
