import React, {ReactElement, useEffect, useState} from 'react';
import {getWeatherInfo} from '../effects/weather';
import {WeatherForecastListType} from '../types/weatherForecastList.type';
import {useMounted} from '../../shared/hooks/useMounted';

const Weather = (): ReactElement => {
  const [weather, setWeather] = useState<WeatherForecastListType | null>(null);
  const isMounted = useMounted();

  useEffect(() => {
    const callGetWeather = async () => {
      const weather = await getWeatherInfo();

      if (isMounted) {
        setWeather(weather);
      }
    }

    callGetWeather();
  }, [setWeather, isMounted]);

  const renderForecast = weather?.forecast.map(forecastItem => (
    <tr key={forecastItem.time}>
      <td>{forecastItem.time}</td>
      <td>{forecastItem.cloudiness}</td>
      <td>{forecastItem.feelsLike}</td>
      <td>{forecastItem.humidity}</td>
      <td>{forecastItem.pressure}</td>
      <td>{forecastItem.temperature}</td>
      <td>{forecastItem.temperatureMax}</td>
      <td>{forecastItem.temperatureMin}</td>
      <td>{forecastItem.windSpeed}</td>
    </tr>
  ));

  return (
    <>
      {
        weather ?
          <>
            <div>
              <p>City: {weather.today.city}</p>
              <p>Cloudiness: {weather.today.cloudiness} %</p>
              <p>Feels like: {weather.today.feelsLike} &#8451;</p>
              <p>Humidity: {weather.today.humidity} %</p>
              <p>Pressure: {weather.today.pressure} hPa</p>
              <p>Temperature: {weather.today.temperature} &#8451;</p>
              <p>Temperature max: {weather.today.temperatureMax} &#8451;</p>
              <p>Temperature min: {weather.today.temperatureMin} &#8451;</p>
              <p>Wind speed: {weather.today.windSpeed} m/s</p>
            </div>
            <table>
              <thead>
              <tr>
                <th>Time</th>
                <th>Cloudiness, %</th>
                <th>Feels like, &#8451;</th>
                <th>Humidity, %</th>
                <th>Pressure, hPa</th>
                <th>Temperature, &#8451;</th>
                <th>Temperature max, &#8451;</th>
                <th>Temperature min, &#8451;</th>
                <th>Wind speed m,/s</th>
              </tr>
              </thead>
              <tbody>
              {renderForecast}
              </tbody>
            </table>
          </> :
          "No weather info"
      }
    </>
  );
};

export default Weather;
