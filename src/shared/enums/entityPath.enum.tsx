export enum EntityPathEnum {
  expenses = 'expenses',
  userExpenses = 'user-expenses',
  incomes = 'incomes',
  userIncomes = 'user-incomes',
  expenseCategories = 'expense-categories',
  userExpenseCategories = 'user-expense-categories',
  typesOfSaving = 'types-of-saving',
  userTypesOfSaving = 'user-types-of-saving',
  typeOfSavingTransfers = 'type-of-income-transfers',
  userTypeOfSavingTransfers = 'user-type-of-saving-transfers',
  users = 'users',
}
