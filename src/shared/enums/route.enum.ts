export enum RouteEnum {
  expenses = 'expenses',
  incomes = 'incomes',
  expenseCategories = 'expense-categories',
  weather = 'weather',
  currencyExchange = 'currency-exchange',
  poezdatoSchedule = 'poezdato-schedule',
  currencies = 'currencies',
  typesOfSaving = 'types-of-saving',
  typeOfSavingTransfers = 'types-of-saving-transfers',
  resetPassword = 'reset-password',
  signIn = 'sign-in',
  signUp = 'sign-up',
}
