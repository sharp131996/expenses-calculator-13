type CurrencyList = {
  [key: string]: string;
}

export const CURRENCY_NAME: CurrencyList = {
  AZN: 'Azerbaijani manat',
  BYN: 'Belarusian ruble',
  CAD: 'Canadian dollar',
  CHF: 'Swiss franc',
  CNY: 'Chinese Yuan',
  CZK: 'Czech koruna',
  DKK: 'Danish krone',
  EUR: 'Euro',
  GBP: 'Pound sterling',
  GEL: 'Georgian lari',
  HUF: 'Hungarian forint',
  ILS: 'Israeli new shekel',
  JPY: 'Japanese yen',
  KZT: 'Kazakhstani tenge',
  MDL: 'Moldovan leu',
  NOK: 'Norwegian krone',
  PLN: 'Polish złoty',
  RUB: 'Russian ruble',
  SEK: 'Swedish krona',
  SGD: 'Singapore dollar',
  TMT: 'Turkmenistani manat',
  TRY: 'Turkish lira',
  UAH: 'Ukraine hryvnia',
  USD: 'US dollar',
  UZS: 'Uzbekistani soʻm',
};
