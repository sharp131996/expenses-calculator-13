export const DATE_INPUT_FORMAT = 'yyyy-MM-DD';
export const TABLE_DATE_FORMAT = 'DD-MM-YYYY';
export const MONTH_DATE_FORMAT = 'yyyy-MM';
