export type FirestoreCollectionType<T> = {
  path: string;
  value: T;
  ids: string[];
  isLoading: boolean;
}
