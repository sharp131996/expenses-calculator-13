import {RouteEnum} from '../enums/route.enum';

export const redirectToRelatedPage = (isUserLoggedIn: boolean) => {
  const outerPages: string[] = [
    RouteEnum.resetPassword,
    RouteEnum.signIn,
    RouteEnum.signUp,
    '',
  ];
  const innerPages: string[] = [
    RouteEnum.expenses,
    RouteEnum.incomes,
    RouteEnum.expenseCategories,
    RouteEnum.weather,
    RouteEnum.currencyExchange,
    RouteEnum.poezdatoSchedule,
    RouteEnum.currencies,
    RouteEnum.typesOfSaving,
    RouteEnum.typeOfSavingTransfers,
    '',
  ];
  const pathname = window.location.pathname.split('/')[1];
  const isMakeRedirectToSignIn = !isUserLoggedIn && innerPages.includes(pathname);
  const isMakeRedirectToExpenses = isUserLoggedIn && outerPages.includes(pathname);

  if (isMakeRedirectToSignIn) {
    window.location.href = `/${RouteEnum.signIn}`;
  }

  if (isMakeRedirectToExpenses) {
    window.location.href = `/${RouteEnum.expenses}`;
  }
};
