import {ExpenseCategoryType} from '../../expense-categories/types/expenseCategory.type';

export const sortCategoriesByPosition = (a: ExpenseCategoryType, b: ExpenseCategoryType): number =>
  a.position - b.position;
