import moment from 'moment';

export const setDateTime = (date: number | string = Number(moment()), savedDate?: number): number => {
  const now = savedDate ? moment(savedDate) : moment();
  const changedDate = moment(date)
    .add(now.get('h'), 'h')
    .add(now.get('m'), 'm')
    .add(now.get('s'), 's');

  return Number(changedDate);
}
