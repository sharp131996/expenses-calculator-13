import {EntityPathEnum} from '../enums/entityPath.enum';
import firebase from 'firebase';

export const setTypeOfSavingAmount = async (key: string, amount: number): Promise<void> => {
  const db = firebase.firestore();
  const userKey = firebase.auth().currentUser?.uid;

  await db.collection(EntityPathEnum.typesOfSaving)
    .doc(userKey)
    .collection(EntityPathEnum.userTypesOfSaving)
    .doc(key)
    .update({ amount: Number(amount) });
}
