import {round} from './round';
import {TypeOfSavingType} from '../../type-of-saving/types/typeOfSaving.type';

export const calculateAmountInBaseCurrency = (baseCurrencyKey: string, amount: number, rate: number, typeOfSaving: TypeOfSavingType): number => {
  return baseCurrencyKey === typeOfSaving?.currencyKey ?
    Number(amount) :
    round(amount * rate)
};
