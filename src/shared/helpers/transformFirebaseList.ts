import firebase from "firebase";

export function transformFirebaseList<T> (collectionList: firebase.firestore.QuerySnapshot): T[] {
  const transformedList: T[] = [];

  collectionList.forEach((snapshot) => {
    const expensePayload = snapshot.data() as T;

    transformedList.push({
      key: snapshot.ref.id,
      ...expensePayload,
    })
  });

  return transformedList;
}
