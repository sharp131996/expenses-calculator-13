import React, {forwardRef, ReactElement, Ref, useContext, useEffect} from 'react';
import {CURRENCY_NAME} from '../consts/currencyName';
import {CurrencyExchangeContext, UserInfoContext} from '../../App';

type CurrencySelectProps = {
  setInitialCurrencyKey?(baseCurrencyKey: string): void;
  isDisabled?: boolean;
}

const CurrencySelect = forwardRef((
  {
    setInitialCurrencyKey,
    isDisabled,
    ...props
  }: CurrencySelectProps,
  ref: Ref<HTMLSelectElement>,
): ReactElement => {
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const { rates } = useContext(CurrencyExchangeContext);
  const currencyExchangeRate = Object.keys(rates);

  useEffect(() => {
    if (Object.keys(rates).length && baseCurrencyKey && setInitialCurrencyKey) {
      setInitialCurrencyKey(baseCurrencyKey);
    }
  }, [rates, baseCurrencyKey, setInitialCurrencyKey]);

  const renderCurrencyOptions = currencyExchangeRate.map((rateKey): ReactElement => (
    <option key={rateKey} value={rateKey}>{CURRENCY_NAME[rateKey] || rateKey}</option>
  ));

  return (
    <select
      {...props}
      ref={ref}
      disabled={isDisabled}
    >
      {renderCurrencyOptions}
    </select>
  )
});

export default CurrencySelect;
