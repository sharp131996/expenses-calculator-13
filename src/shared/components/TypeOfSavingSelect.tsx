import React, {forwardRef, ReactElement, Ref, useContext, useEffect} from 'react';
import {TypesOfSavingContext, UserInfoContext} from '../../App';
import {CURRENCY_NAME} from '../consts/currencyName';

type TypeOfSavingSelectProps = {
  setValue?(baseCurrencyKey: string): void;
  getValues?(baseCurrencyKey: string): string;
  typeOfSavingSelectKey: string;
}

const TypeOfSavingSelect = forwardRef((
  {
    setValue,
    typeOfSavingSelectKey,
    ...props
  }: TypeOfSavingSelectProps,
  ref: Ref<HTMLSelectElement>
): ReactElement => {
  const { typesOfSaving } = useContext(TypesOfSavingContext);
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);
  const typesOfSavingKeys = Object.keys(typesOfSaving);

  useEffect(() => {
    if (typesOfSavingKeys.length && setValue && !typeOfSavingSelectKey) {
      const typeOfSavingKey = typesOfSavingKeys.find((key) => {
        const typeOfSaving = typesOfSaving[key];

        return typeOfSaving.currencyKey === baseCurrencyKey;
      });

      setValue(typeOfSavingKey || typesOfSavingKeys[0]);
    }
  }, [typesOfSavingKeys, typesOfSaving, setValue, typeOfSavingSelectKey, baseCurrencyKey]);

  const renderCurrencyOptions = typesOfSavingKeys.map((savingKey): ReactElement => (
    <option key={savingKey} value={savingKey}>
      {typesOfSaving[savingKey]?.name || savingKey} {CURRENCY_NAME[typesOfSaving[savingKey]?.currencyKey] || typesOfSaving[savingKey]?.currencyKey}
    </option>
  ));

  return (
    <select
      {...props}
      ref={ref}
    >
      {renderCurrencyOptions}
    </select>
  )
});

export default TypeOfSavingSelect;
