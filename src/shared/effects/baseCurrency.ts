import {EntityPathEnum} from '../enums/entityPath.enum';
import {UserInfoType} from '../types/userInfo.type';
import firebase from 'firebase';

export const getUserInfoFirestore = async (userKey: string): Promise<UserInfoType> => {
  const db = firebase.firestore();

  const userInfoSnapshot = await db.collection(EntityPathEnum.users)
    .doc(userKey)
    .get();

  return userInfoSnapshot.data() as UserInfoType;
}
