export type RateType = {
  [key: string]: {
    saleRate: number;
    purchaseRate: number;
  }
};

export type CurrencyExchangeType = {
  date: string;
  rates: RateType;
};
