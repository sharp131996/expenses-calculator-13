export type CurrencyExchangeFormType = {
  currencyKey: string;
  baseCurrencyAmount: number;
  secondaryCurrencyAmount: number;
};
