import axios from 'axios';
import {CurrencyExchangeType} from '../types/currencyExchange.type';
import {BACK_END_URL} from '../../shared/consts/backEndUrl.const';

const CURRENCY_EXCHANGE_URL = `${BACK_END_URL}currency-exchange`;

export const getCurrencyExchangeApi = async (): Promise<CurrencyExchangeType> => {
  const { data: currencyExchange } = await axios(CURRENCY_EXCHANGE_URL);

  return currencyExchange;
}
