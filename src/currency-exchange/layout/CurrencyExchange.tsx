import React, {ReactElement, SyntheticEvent, useContext, useEffect} from 'react';
import {CurrencyExchangeContext} from '../../App';
import {useForm} from 'react-hook-form';
import {CurrencyExchangeFormType} from '../types/currencyExchangeForm.type';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';
import CurrencySelect from '../../shared/components/CurrencySelect';

const CurrencyExchange = (): ReactElement => {
  const { rates } = useContext(CurrencyExchangeContext);
  const { register, setValue, getValues, reset, watch } = useForm<CurrencyExchangeFormType>({
    defaultValues: {
      baseCurrencyAmount: 1,
      secondaryCurrencyAmount: 0,
    }
  });
  const currencyKey = watch('currencyKey');

  useEffect(() => {
    if (Object.keys(rates).length) {
      const rate = rates['USD'];
      const calculatedSecondaryCurrencyAmount = rate.saleRate;
      setValue('secondaryCurrencyAmount', calculatedSecondaryCurrencyAmount);
      setValue('currencyKey', 'USD');
    }
  }, [rates, setValue]);

  useEffect(() => {
    const rate = rates[currencyKey];

    if (rate) {
      setValue('secondaryCurrencyAmount', rate.saleRate);
      setValue('baseCurrencyAmount', 1);
    }
  }, [currencyKey, rates, setValue]);


  const onBaseCurrencyAmountChange = (event: SyntheticEvent): void => {
    const value = Number((event.target as HTMLInputElement).value);
    const rate = rates[getValues('currencyKey')];
    const calculatedSecondaryCurrencyAmount = chain(value).multiply(rate.purchaseRate).format(PRECISION).done();

    setValue('secondaryCurrencyAmount', calculatedSecondaryCurrencyAmount);
  };

  const onSecondaryCurrencyAmountChange = (event: SyntheticEvent): void => {
    const value = Number((event.target as HTMLInputElement).value);
    const rate = rates[getValues('currencyKey')];
    const calculatedSecondaryCurrencyAmount = chain(value).multiply(rate.saleRate).format(PRECISION).done();

    setValue('baseCurrencyAmount', calculatedSecondaryCurrencyAmount);
  };

  const onReset = () => {
    reset();
  };

  return (
    <form autoComplete="off">
      <label>
        UAH

        <input
          type="number"
          step="0.01"
          {...register('baseCurrencyAmount')}
          onChange={onBaseCurrencyAmountChange}
        />
      </label>

      <label>
        <input
          type="number"
          step="0.01"
          {...register('secondaryCurrencyAmount')}
          onChange={onSecondaryCurrencyAmountChange}
        />
      </label>

      <CurrencySelect
        {...register('currencyKey', { required: true })}
      />

      <button onClick={onReset}>Reset</button>
    </form>
  );
};

export default CurrencyExchange;
