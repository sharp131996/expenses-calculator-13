import {TypeOfSavingCollectionType} from '../types/typeOfSavingCollection.type';
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {transformFirebaseList} from '../../shared/helpers/transformFirebaseList';
import {TypeOfSavingType} from '../types/typeOfSaving.type';

export const getTypesOfSavingFirestore = async (userKey: string): Promise<TypeOfSavingCollectionType> => {
  const db = firebase.firestore();
  const typesOfSavingCollection: TypeOfSavingCollectionType = {};

  const typesOfSavingSnapshot = await db.collection(EntityPathEnum.typesOfSaving)
    .doc(userKey)
    .collection(EntityPathEnum.userTypesOfSaving)
    .orderBy('createdAt', 'desc')
    .get();

  const transformedTypesOfSaving = transformFirebaseList<TypeOfSavingType>(typesOfSavingSnapshot);

  transformedTypesOfSaving.forEach((source) => {
    if (source.key) {
      typesOfSavingCollection[source.key] = source;
    }
  });

  return typesOfSavingCollection;
};
