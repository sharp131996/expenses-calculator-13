import {TypeOfSavingType} from './typeOfSaving.type';

export type TypeOfSavingCollectionType = {
  [key: string]: TypeOfSavingType;
}
