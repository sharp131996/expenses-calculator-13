export type RegisterOptionsType = {
  required?: boolean,
  valueAsNumber?: boolean,
};
