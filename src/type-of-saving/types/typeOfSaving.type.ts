export type TypeOfSavingType = {
  key?: string;
  name: string;
  currencyKey: string;
  amount: number;
  comment: string;
  createdAt: number;
}
