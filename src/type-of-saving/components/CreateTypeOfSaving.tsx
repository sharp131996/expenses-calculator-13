import React, {ReactElement, useContext, useState} from 'react';
import {useForm} from "react-hook-form";
import firebase from "firebase/app";
import 'firebase/firestore';
import moment from 'moment';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {TypeOfSavingType} from '../types/typeOfSaving.type';
import {TypeOfSavingForm} from './TypeOfSavingForm';
import {useMounted} from '../../shared/hooks/useMounted';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {TypesOfSavingContext, UserContext} from '../../App';

const CreateTypeOfSaving = (): ReactElement => {
  const { getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const db = firebase.firestore();
  const { register, handleSubmit, reset, setValue } = useForm<TypeOfSavingType>({
    defaultValues: {
      createdAt: Number(new Date()),
    }
  });

  const onSubmit = async (typeOfSaving: TypeOfSavingType): Promise<void> => {
    setLoading(true);

    typeOfSaving.createdAt = setDateTime(Number(moment()));

    await db.collection(EntityPathEnum.typesOfSaving)
      .doc(user.key)
      .collection(EntityPathEnum.userTypesOfSaving)
      .doc()
      .set(typeOfSaving);
    if (isMounted) {
      await getTypesOfSaving(user.key);
    }
    reset();

    setLoading(false);
  }

  return <TypeOfSavingForm
    isCreate
    register={register}
    onSubmit={handleSubmit(onSubmit)}
    setValue={setValue}
    loading={loading}
  />;
}

export default CreateTypeOfSaving;
