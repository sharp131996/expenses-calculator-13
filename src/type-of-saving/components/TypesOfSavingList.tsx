import React, {ReactElement, useContext, useState} from 'react';
import {TypeOfSavingType} from '../types/typeOfSaving.type';
import DeleteTypeOfSaving from './DeleteTypeOfSaving';
import EditTypeOfSaving from './EditTypeOfSaving';
import {CurrencyExchangeContext, TypesOfSavingContext, UserInfoContext} from '../../App';
import {CURRENCY_NAME} from '../../shared/consts/currencyName';
import {calculateAmountInBaseCurrency} from '../../shared/helpers/calculateAmountInBaseCurrency';
import {chain} from 'mathjs';
import {PRECISION} from '../../shared/consts/precision.const';

const TypesOfSavingList = (): ReactElement => {
  const { typesOfSaving } = useContext(TypesOfSavingContext);
  const { rates } = useContext(CurrencyExchangeContext);
  const { userInfo: { baseCurrencyKey } } = useContext(UserInfoContext);

  const totalAmount = Object.keys(typesOfSaving).reduce((prevAmount: number, key: string) => {
    const typeOfSaving = typesOfSaving[key];
    const rate = rates && rates[typeOfSaving.currencyKey]?.saleRate;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, typeOfSaving.amount, rate, typeOfSaving);

    return Number(chain(prevAmount).add(amountInBaseCurrency).format(PRECISION).done());
  }, 0);

  const isTypesOfSavingExist = Object.keys(Object.keys(typesOfSaving)).length;

  const [editTypeOfSaving, setEditTypeOfSaving] = useState<TypeOfSavingType | null>();

  const resetEditTypeOfSaving = (): void => {
    setEditTypeOfSaving(null);
  };

  const typesOfSavingList = isTypesOfSavingExist && Object.keys(typesOfSaving).map((key: string, index: number) => {
    const typeOfSaving = typesOfSaving[key];
    const rate = rates && rates[typeOfSaving.currencyKey]?.saleRate;
    const amountInBaseCurrency = calculateAmountInBaseCurrency(baseCurrencyKey, typeOfSaving.amount, rate, typeOfSaving);

    return (
      <tr key={key ? key : 1}>
        <td>{index + 1}</td>
        <td>{typeOfSaving.name}</td>
        <td>{typeOfSaving.amount}</td>
        <td>{CURRENCY_NAME[typeOfSaving.currencyKey] || typeOfSaving.currencyKey}</td>
        <td>{amountInBaseCurrency}</td>
        <td>{typeOfSaving.comment}</td>
        <td>
          <button onClick={() => {
            setEditTypeOfSaving({...typeOfSaving, key});
          }}>Edit</button>
          <DeleteTypeOfSaving
            typeOfSavingId={key}
          />
        </td>
      </tr>
    );
  });

  return (
    <>
      {
        editTypeOfSaving ?
          <EditTypeOfSaving
            typeOfSaving={editTypeOfSaving}
            resetEditTypeOfSaving={resetEditTypeOfSaving}
          /> :
          null
      }

      {isTypesOfSavingExist ?
        <>
          <h1>Categories table</h1>

          Total: {totalAmount} {baseCurrencyKey}
          <table>
            <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Amount</th>
              <th>Currency</th>
              <th>Amount in base currency</th>
              <th>Comment</th>
            </tr>
            </thead>
            <tbody>
            {typesOfSavingList}
            </tbody>
          </table>
        </> :
        "No types of saving"
      }
    </>
  );
}

export default TypesOfSavingList;
