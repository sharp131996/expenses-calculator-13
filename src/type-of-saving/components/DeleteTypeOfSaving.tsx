import React, {ReactElement, useContext, useState} from 'react';
import firebase from "firebase/app";
import 'firebase/firestore';
import {EntityPathEnum} from "../../shared/enums/entityPath.enum";
import {useMounted} from '../../shared/hooks/useMounted';
import {TypesOfSavingContext, UserContext} from '../../App';

type DeleteTypeOfSavingProps = {
  typeOfSavingId: string;
}

const DeleteTypeOfSaving = ({typeOfSavingId}: DeleteTypeOfSavingProps): ReactElement => {
  const { getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const onDelete = async (): Promise<void> => {
    setLoading(true);

    await db
      .collection(EntityPathEnum.typesOfSaving)
      .doc(user.key)
      .collection(EntityPathEnum.userTypesOfSaving)
      .doc(typeOfSavingId)
      .delete();
    if (isMounted) {
      await getTypesOfSaving(user.key);
    }

    setLoading(false);
  };

  return <button onClick={onDelete} disabled={loading}>Delete</button>;
}

export default DeleteTypeOfSaving;
