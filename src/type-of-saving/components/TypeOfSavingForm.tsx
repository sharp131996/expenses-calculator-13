import React, {ReactElement} from 'react';
import {UseFormRegisterReturn, UseFormSetValue} from 'react-hook-form/dist/types/form';
import CurrencySelect from '../../shared/components/CurrencySelect';
import {TypeOfSavingType} from '../types/typeOfSaving.type';
import {RegisterOptionsType} from '../types/registerOptions.type';

type TypeOfSavingFormProps = {
  isCreate?: boolean;
  register(fieldName: string, options?: RegisterOptionsType): UseFormRegisterReturn;
  onSubmit(): Promise<void>;
  setValue: UseFormSetValue<TypeOfSavingType>;
  loading: boolean;
}

export const TypeOfSavingForm = (
  {
    isCreate,
    register,
    onSubmit,
    setValue,
    loading,
  }: TypeOfSavingFormProps
): ReactElement => {
  const caption = isCreate ? 'Create' : 'Edit';

  const setSelectCurrency = (currencyKey: string) => setValue('currencyKey', currencyKey);

  return (
    <form onSubmit={onSubmit} autoComplete="off">
      <h1>{caption} type of saving</h1>
      <input
        {...register("name", { required: true })}
        placeholder="Name"
      />
      <CurrencySelect
        {...register('currencyKey')}
        setInitialCurrencyKey={isCreate ? setSelectCurrency : undefined}
      />
      <input
        type="number"
        step="0.01"
        {...register("amount", { required: true, valueAsNumber: true })}
        placeholder="Amount"
      />
      <input
        {...register("comment")}
        placeholder="Comment"
      />

      <button type="submit" disabled={loading}>{caption}</button>
    </form>
  )
}
