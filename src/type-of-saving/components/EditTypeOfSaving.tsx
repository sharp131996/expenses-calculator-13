import React, {ReactElement, useContext, useEffect, useState} from 'react';
import {useForm} from "react-hook-form";
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {TypeOfSavingType} from '../types/typeOfSaving.type';
import {TypeOfSavingForm} from './TypeOfSavingForm';
import {useMounted} from '../../shared/hooks/useMounted';
import {TypesOfSavingContext, UserContext} from '../../App';

type EditTypeOfSavingProps = {
  typeOfSaving: TypeOfSavingType;
  resetEditTypeOfSaving: () => void;
}

const EditTypeOfSaving = ({typeOfSaving, resetEditTypeOfSaving}: EditTypeOfSavingProps): ReactElement => {
  const { getTypesOfSaving } = useContext(TypesOfSavingContext);
  const { user } = useContext(UserContext);

  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const { register, handleSubmit, reset, setValue } = useForm<TypeOfSavingType>({
    defaultValues: {
      ...typeOfSaving,
    }
  });

  useEffect(() => {
    setValue("name", typeOfSaving.name);
    setValue("currencyKey", typeOfSaving.currencyKey);
    setValue("amount", typeOfSaving.amount);
    setValue("comment", typeOfSaving.comment);
  }, [setValue, typeOfSaving]);

  const onSubmit = async (typeOfSaving: TypeOfSavingType): Promise<void> => {
    setLoading(true);

    await db.collection(EntityPathEnum.typesOfSaving)
      .doc(user.key)
      .collection(EntityPathEnum.userTypesOfSaving)
      .doc(typeOfSaving.key)
      .update(typeOfSaving);

    if (isMounted) {
      await getTypesOfSaving(user.key);
      resetEditTypeOfSaving();
    }

    reset();

    setLoading(false);
  }

  return <TypeOfSavingForm
    register={register}
    onSubmit={handleSubmit(onSubmit)}
    setValue={setValue}
    loading={loading}
  />;
}

export default EditTypeOfSaving;
