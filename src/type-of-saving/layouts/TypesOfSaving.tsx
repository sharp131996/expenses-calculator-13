import React, {ReactElement} from 'react';
import CreateTypeOfSaving from '../components/CreateTypeOfSaving';
import TypesOfSavingList from '../components/TypesOfSavingList';

const TypesOfSaving = (): ReactElement => {
  return (
    <>
      <CreateTypeOfSaving />
      <TypesOfSavingList />
    </>
  );
};

export default TypesOfSaving;
