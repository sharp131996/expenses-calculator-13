import React, {ReactElement, useContext} from 'react';
import firebase from 'firebase';
import SignUp from './auth/layouts/SignUp';
import {Link, Route, Routes, useNavigate} from 'react-router-dom';
import {RouteEnum} from './shared/enums/route.enum';
import Expenses from './expenses/layouts/Expenses';
import Incomes from './incomes/layouts/Incomes';
import ExpenseCategories from './expense-categories/layouts/ExpenseCategories';
import Weather from './weather/layouts/Weather';
import CurrencyExchange from './currency-exchange/layout/CurrencyExchange';
import PoezdatoSchedule from './poezdato-schedule/layouts/PoezdatoSchedule';
import Currencies from './currencies/layouts/Currencies';
import TypesOfSaving from './type-of-saving/layouts/TypesOfSaving';
import ResetPassword from './auth/layouts/ResetPassword';
import SingIn from './auth/layouts/SingIn';
import {UserContext, UserInfoContext} from './App';

const Layout = (): ReactElement => {
  const auth = firebase.auth();
  const { user } = useContext(UserContext);
  const { userInfo } = useContext(UserInfoContext);
  const isUserReceivedAllInfo = user.key && userInfo?.baseCurrencyKey;
  const navigate = useNavigate();

  const signOut = async () => {
    await auth.signOut();
    navigate(`/${RouteEnum.signIn}`);
  };

  return (
    <>
      {
        user.key && (
          <button onClick={signOut}>Sign out</button>
        )
      }

      <nav>
        <ul>
          {
            isUserReceivedAllInfo && (
              <>
                <li>
                  <Link to={RouteEnum.expenses}>Expenses</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.incomes}`}>Incomes</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.expenseCategories}`}>Expense categories</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.weather}`}>Weather</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.currencyExchange}`}>Currency exchange</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.poezdatoSchedule}`}>Poezdato Schedule</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.currencies}`}>Currencies</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.typesOfSaving}`}>Types of saving</Link>
                </li>
              </>
            )
          }
          {
            !user.key && (
              <>
                <li>
                  <Link to={`/${RouteEnum.signIn}`}>Sign in</Link>
                </li>
                <li>
                  <Link to={`/${RouteEnum.signUp}`}>Sign up</Link>
                </li>
              </>
            )
          }
          <li>
            <Link to={`/${RouteEnum.resetPassword}`}>Reset password</Link>
          </li>
        </ul>
      </nav>

      <Routes>
        {
          isUserReceivedAllInfo && (
            <>
              <Route path={RouteEnum.expenses} element={<Expenses />} />
              <Route path={RouteEnum.incomes} element={<Incomes />} />
              <Route path={RouteEnum.expenseCategories} element={<ExpenseCategories />} />
              <Route path={RouteEnum.weather} element={<Weather />} />
              <Route path={RouteEnum.currencyExchange} element={<CurrencyExchange />} />
              <Route path={RouteEnum.poezdatoSchedule} element={<PoezdatoSchedule />} />
              <Route path={RouteEnum.currencies} element={<Currencies />} />
              <Route path={RouteEnum.typesOfSaving} element={<TypesOfSaving />} />
            </>
          )
        }
        {
          !user.key && (
            <>
              <Route path={RouteEnum.signIn} element={<SingIn />} />
              <Route path={RouteEnum.signUp} element={<SignUp />} />
            </>
          )
        }
        <Route path={RouteEnum.resetPassword} element={<ResetPassword />} />
      </Routes>
    </>
  );
};

export default Layout;
