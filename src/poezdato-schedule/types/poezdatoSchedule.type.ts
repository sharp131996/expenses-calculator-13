export type ScheduleType = {
  title: string;
  from: string;
  to: string;
  startTime: string;
  endTime: string;
};

export type PoezdatoScheduleType = {
  vishnevoeKyivPass: ScheduleType[],
  kyivPassVishnevoe: ScheduleType[],
}
