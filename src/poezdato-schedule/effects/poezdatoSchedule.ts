import axios from 'axios';
import {BACK_END_URL} from '../../shared/consts/backEndUrl.const';
import {PoezdatoScheduleType} from '../types/poezdatoSchedule.type';

const CURRENCY_EXCHANGE_URL = `${BACK_END_URL}poezdato-schedule`;

export const getPoezdatoSchedule = async (): Promise<PoezdatoScheduleType> => {
  const { data: currencyExchange } = await axios(CURRENCY_EXCHANGE_URL);

  return currencyExchange;
}
