import React, {ReactElement, useEffect, useState} from 'react';
import {PoezdatoScheduleType, ScheduleType} from '../types/poezdatoSchedule.type';
import {getPoezdatoSchedule} from '../effects/poezdatoSchedule';
import {useMounted} from '../../shared/hooks/useMounted';

const tableHead = (
  <thead>
  <tr>
    <td>Start time</td>
    <td>End time</td>
    <td>Title</td>
    <td>To</td>
    <td>From</td>
  </tr>
  </thead>
)

const PoezdatoSchedule = (): ReactElement => {
  const isMounted = useMounted();
  const [poezdatoSchedule, setPoezdatoSchedule] = useState<PoezdatoScheduleType>({
    kyivPassVishnevoe: [],
    vishnevoeKyivPass: [],
  });

  useEffect(() => {
    const getPoezdatoScheduleCall = async () => {
      const poezdatoScheduleList = await getPoezdatoSchedule();

      if (isMounted) {
        setPoezdatoSchedule(poezdatoScheduleList);
      }
    };

    getPoezdatoScheduleCall();
  }, [isMounted]);

  const renderScheduleItem = (schedule: ScheduleType, i: number): ReactElement => (
    <tr key={i}>
      <td>{schedule.startTime}</td>
      <td>{schedule.endTime}</td>
      <td>{schedule.title}</td>
      <td>{schedule.to}</td>
      <td>{schedule.from}</td>
    </tr>
  );

  const renderPoezdatoVishnevoeKyivPassSchedule = poezdatoSchedule.vishnevoeKyivPass.map(renderScheduleItem)
  const renderPoezdatoKyivPassVishnevoeSchedule = poezdatoSchedule.kyivPassVishnevoe.map(renderScheduleItem);

  return (
    <>
      <h2>Vishnevoe--Kyiv Pass</h2>
      <table>
        {tableHead}
        <tbody>
        {renderPoezdatoVishnevoeKyivPassSchedule}
        </tbody>
      </table>

      <h2>Kyiv Pass--Vishnevoe</h2>
      <table>
        {tableHead}
        <tbody>
        {renderPoezdatoKyivPassVishnevoeSchedule}
        </tbody>
      </table>
    </>
  );
};

export default PoezdatoSchedule;
