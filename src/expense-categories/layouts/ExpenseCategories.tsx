import React, {ReactElement} from 'react';
import CreateExpenseCategory from '../components/CreateExpenseCategory';
import ExpenseCategoriesList from '../components/ExpenseCategoriesList';

const ExpensesCategories = (): ReactElement => {
  return (
    <>
      <CreateExpenseCategory />
      <ExpenseCategoriesList />
    </>
  );
};

export default ExpensesCategories;
