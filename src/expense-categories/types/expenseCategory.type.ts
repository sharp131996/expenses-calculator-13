export type ExpenseCategoryType = {
  key: string;
  name: string;
  createdAt: number;
  position: number;
}
