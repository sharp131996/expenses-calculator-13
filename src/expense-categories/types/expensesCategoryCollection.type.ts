import {ExpenseCategoryType} from './expenseCategory.type';

export type ExpensesCategoryCollectionType = {
  [key: string]: ExpenseCategoryType;
}
