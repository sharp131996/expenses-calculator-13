import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import firebase from 'firebase';
import {ExpenseCategoryType} from '../types/expenseCategory.type';
import {ExpensesCategoryCollectionType} from '../types/expensesCategoryCollection.type';
import {transformFirebaseList} from '../../shared/helpers/transformFirebaseList';

export const getExpenseCategoriesFirestore = async (userKey: string): Promise<ExpensesCategoryCollectionType> => {
  const db = firebase.firestore();
  const expenseCategoriesCollection: ExpensesCategoryCollectionType = {};

  const expenseCategoriesSnapshot = await db.collection(EntityPathEnum.expenseCategories)
    .doc(userKey)
    .collection(EntityPathEnum.userExpenseCategories)
    .orderBy('createdAt', 'desc')
    .get();

  const transformedExpenseCategories = transformFirebaseList<ExpenseCategoryType>(expenseCategoriesSnapshot);

  transformedExpenseCategories.forEach((category) => {
    if (category.key) {
      expenseCategoriesCollection[category.key] = category;
    }
  });

  return expenseCategoriesCollection;
}
