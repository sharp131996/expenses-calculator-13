import React, {ReactElement} from 'react';
import {UseFormRegisterReturn} from 'react-hook-form/dist/types/form';

type ExpenseCategoryFormType = {
  isCreate?: boolean;
  register(fieldName: string, options?: { required: boolean }): UseFormRegisterReturn;
  onSubmit(): Promise<void>;
  loading: boolean;
}

export const ExpenseCategoryForm = ({isCreate, register, onSubmit, loading}: ExpenseCategoryFormType): ReactElement => {
  const caption = isCreate ? 'Create' : 'Edit';

  return (
    <form onSubmit={onSubmit} autoComplete="off">
      <h1>{caption} category</h1>
      <input
        {...register("name", { required: true })}
        placeholder="Category name"
      />

      <button type="submit" disabled={loading}>{caption}</button>
    </form>
  )
}
