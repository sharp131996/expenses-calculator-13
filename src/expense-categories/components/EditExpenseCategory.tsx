import React, {ReactElement, useContext, useEffect, useState} from 'react';
import {useForm} from "react-hook-form";
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {ExpenseCategoryForm} from './ExpenseCategoryForm';
import {ExpenseCategoryType} from '../types/expenseCategory.type';
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpenseCategoriesContext, UserContext} from '../../App';

type EditExpenseCategoryType = {
  expenseCategory: ExpenseCategoryType;
  resetEditExpenseCategory: () => void;
}

const EditExpenseCategory = ({expenseCategory, resetEditExpenseCategory}: EditExpenseCategoryType): ReactElement => {
  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const { user } = useContext(UserContext);
  const { getExpenseCategories } = useContext(ExpenseCategoriesContext);
  const { register, handleSubmit, reset, setValue } = useForm<ExpenseCategoryType>({
    defaultValues: {
      ...expenseCategory,
    }
  });

  useEffect(() => {
    setValue("name", expenseCategory.name);
  }, [setValue, expenseCategory]);

  const onSubmit = async (expenseCategory: ExpenseCategoryType): Promise<void> => {
    setLoading(true);

    await db.collection(EntityPathEnum.expenseCategories)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenseCategories)
      .doc(expenseCategory.key)
      .update(expenseCategory);

    if (isMounted) {
      await getExpenseCategories(user.key);
      resetEditExpenseCategory();
    }
    reset();

    setLoading(false);
  }

  return <ExpenseCategoryForm
    register={register}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default EditExpenseCategory;
