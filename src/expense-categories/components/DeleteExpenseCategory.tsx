import React, {ReactElement, useContext, useState} from 'react';
import firebase from "firebase/app";
import 'firebase/firestore';
import {EntityPathEnum} from "../../shared/enums/entityPath.enum";
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpenseCategoriesContext, UserContext} from '../../App';

type DeleteExpenseCategoryProps = {
  expenseCategoryId: string;
}

const DeleteExpenseCategory = ({expenseCategoryId}: DeleteExpenseCategoryProps): ReactElement => {
  const db = firebase.firestore();
  const isMounted = useMounted();
  const [loading, setLoading] = useState(false);
  const { getExpenseCategories } = useContext(ExpenseCategoriesContext);
  const { user } = useContext(UserContext);
  const onDelete = async (): Promise<void> => {
    setLoading(true);

    await db
      .collection(EntityPathEnum.expenseCategories)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenseCategories)
      .doc(expenseCategoryId)
      .delete();
    if (isMounted) {
      await getExpenseCategories(user.key);
    }

    setLoading(false);
  };

  return <button onClick={onDelete} disabled={loading}>Delete</button>;
}

export default DeleteExpenseCategory;
