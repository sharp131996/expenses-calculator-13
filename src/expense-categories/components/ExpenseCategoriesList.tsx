import React, {ReactElement, useCallback, useContext, useState} from 'react';
import EditExpenseCategory from './EditExpenseCategory';
import DeleteExpenseCategory from './DeleteExpenseCategory';
import {ExpenseCategoryType} from '../types/expenseCategory.type';
import {ExpenseCategoriesContext, UserContext} from '../../App';
import ReactDragList from 'react-drag-list';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import firebase from 'firebase';
import {sortCategoriesByPosition} from '../../shared/helpers/sortCategoriesByPosition';

const ExpenseCategoriesList = (
): ReactElement => {
  const db = firebase.firestore();
  const { expenseCategories, getExpenseCategories } = useContext(ExpenseCategoriesContext);
  const { user } = useContext(UserContext);
  const [editExpenseCategory, setEditExpenseCategory] = useState<ExpenseCategoryType | null>();
  const sortedExpensesCategories = Object.values(expenseCategories)
    .sort(sortCategoriesByPosition);

  const resetEditExpenseCategory = (): void => {
    setEditExpenseCategory(null);
  };

  const renderExpenseCategory = useCallback(
    (expenseCategory: ExpenseCategoryType) => {
      return (
        <>
          <div>{expenseCategory.name}</div>
          <div>
            <button onClick={() => {
              setEditExpenseCategory({...expenseCategory});
            }}>Edit</button>
            <DeleteExpenseCategory
              expenseCategoryId={String(expenseCategory.key)}
            />
          </div>
        </>
      );
    },
    [],
  );

  const onSortExpenseCategories = (expensesCategoriesKeys: ExpenseCategoryType[]) => {
    expensesCategoriesKeys.forEach(async (expenseCategory: ExpenseCategoryType, position: number) => {
      const transformedExpenseCategory = {
        ...expenseCategory,
        position,
      };

      await db.collection(EntityPathEnum.expenseCategories)
        .doc(user.key)
        .collection(EntityPathEnum.userExpenseCategories)
        .doc(transformedExpenseCategory.key)
        .update(transformedExpenseCategory);
    });

    getExpenseCategories(user.key);
  };

  return (
    <>
      {
        editExpenseCategory ?
          <EditExpenseCategory
            expenseCategory={editExpenseCategory}
            resetEditExpenseCategory={resetEditExpenseCategory}
          /> :
          null
      }

      {sortedExpensesCategories.length ?
        <>
          <h1>Categories</h1>

          <ReactDragList
            dataSource={sortedExpensesCategories}
            row={(record) => renderExpenseCategory(record as ExpenseCategoryType)}
            rowKey={'key'}
            onUpdate={(_, expensesCategoriesKeys) =>
              onSortExpenseCategories(expensesCategoriesKeys as ExpenseCategoryType[])
            }
          />
        </> :
        "No categories"
      }
    </>
  );
}

export default ExpenseCategoriesList;
