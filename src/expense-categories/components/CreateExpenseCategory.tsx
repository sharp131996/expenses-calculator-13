import React, {ReactElement, useContext, useState} from 'react';
import {useForm} from "react-hook-form";
import firebase from "firebase/app";
import 'firebase/firestore';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import {ExpenseCategoryForm} from './ExpenseCategoryForm';
import {ExpenseCategoryType} from '../types/expenseCategory.type';
import {setDateTime} from '../../shared/helpers/setDateTime';
import {useMounted} from '../../shared/hooks/useMounted';
import {ExpenseCategoriesContext, UserContext} from '../../App';

const CreateExpenseCategory = (): ReactElement => {
  const db = firebase.firestore();
  const { register, handleSubmit, reset } = useForm<ExpenseCategoryType>();
  const { getExpenseCategories, expenseCategories } = useContext(ExpenseCategoriesContext);
  const { user } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const isMounted = useMounted();

  const onSubmit = async (expenseCategory: ExpenseCategoryType): Promise<void> => {
    setLoading(true);

    expenseCategory.createdAt = setDateTime(expenseCategory.createdAt);
    expenseCategory.position = expenseCategories ? Object.keys(expenseCategories).length : 1;

    await db.collection(EntityPathEnum.expenseCategories)
      .doc(user.key)
      .collection(EntityPathEnum.userExpenseCategories)
      .doc()
      .set(expenseCategory);

    if (isMounted) {
      await getExpenseCategories(user.key);
    }
    reset();

    setLoading(false);
  }

  return <ExpenseCategoryForm
    isCreate
    register={register}
    onSubmit={handleSubmit(onSubmit)}
    loading={loading}
  />;
}

export default CreateExpenseCategory;
