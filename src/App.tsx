import React, {useCallback, useEffect, useState} from 'react';

import firebase from 'firebase/app';
import 'firebase/firestore';

import {config} from "./config";
import {BrowserRouter} from 'react-router-dom';
import {getUserInfoFirestore} from './shared/effects/baseCurrency';
import {UserInfoType} from './shared/types/userInfo.type';
import {getCurrencyExchangeApi} from './currency-exchange/effects/currencyExchange';
import {CurrencyExchangeType} from './currency-exchange/types/currencyExchange.type';
import {ExpensesCategoryCollectionType} from './expense-categories/types/expensesCategoryCollection.type';
import {getExpenseCategoriesFirestore} from './expense-categories/effects/expensesCategories';
import {ExpenseType} from './expenses/types/expense.type';
import {getExpensesFirestore} from './expenses/effects/expenses';
import {TypeOfSavingCollectionType} from './type-of-saving/types/typeOfSavingCollection.type';
import {getTypesOfSavingFirestore} from './type-of-saving/effects/typesOfSaving';
import {IncomeType} from './incomes/types/income.type';
import {getIncomesFirestore} from './incomes/effects/incomes';
import Layout from './Layout';
import {UserType} from './shared/types/user.type';
import {DEFAULT_BASE_CURRENCY} from './shared/consts/defaultBaseCurrency';
import {redirectToRelatedPage} from './shared/helpers/redirectToRelatedPage';

type InitialUserContextType = {
  user: UserType;
}

const initialUser: UserType = {
  email: '',
  key: '',
}

const initialUserContext: InitialUserContextType = {
  user: initialUser,
}

export const UserContext = React.createContext<InitialUserContextType>(initialUserContext);

type InitialUserInfoContextType = {
  userInfo: UserInfoType;
  getUserInfo: (userKey: string) => Promise<unknown>;
}

const initialUserInfo: UserInfoType = {
  baseCurrencyKey: '',
}
const initialGetUserInfo = () => new Promise<void>(() => {});

const initialUserInfoContext: InitialUserInfoContextType = {
  userInfo: initialUserInfo,
  getUserInfo: initialGetUserInfo,
}

export const UserInfoContext = React.createContext<InitialUserInfoContextType>(initialUserInfoContext);

const initialCurrencyExchange: CurrencyExchangeType = {
  date: '',
  rates: {},
};
export const CurrencyExchangeContext = React.createContext<CurrencyExchangeType>(initialCurrencyExchange);

type InitialExpenseCategories = {
  expenseCategories: ExpensesCategoryCollectionType;
  getExpenseCategories: (userKey: string) => Promise<void>;
}

const initialExpenseCategories: ExpensesCategoryCollectionType = {}
const initialGetExpenseCategories = () => new Promise<void>(() => {});
const initialExpenseCategoriesContext: InitialExpenseCategories = {
  expenseCategories: initialExpenseCategories,
  getExpenseCategories: initialGetExpenseCategories,
}
export const ExpenseCategoriesContext = React.createContext<InitialExpenseCategories>(initialExpenseCategoriesContext);

type InitialExpensesType = {
  expenses: ExpenseType[];
  getExpenses: (userKey: string) => Promise<void>,
  setExpensesFilterDate(filterDate: string): void,
  isShowAllExpenses: boolean,
  setIsShowAllExpenses(isShowAll: boolean): void,
}

const initialGetExpenses = () => new Promise<void>(() => {});
const initialExpensesContext: InitialExpensesType = {
  expenses: [],
  getExpenses: initialGetExpenses,
  setExpensesFilterDate: (filterDate: string): void => {},
  isShowAllExpenses: false,
  setIsShowAllExpenses: (isShowAll: boolean) => {},
}

export const ExpensesContext = React.createContext<InitialExpensesType>(initialExpensesContext);

type InitialIncomesType = {
  incomes: IncomeType[];
  getIncomes: (userKey: string) => Promise<void>,
  setIncomesFilterDate(filterDate: string): void,
  isShowAllIncomes: boolean,
  setIsShowAllIncomes(isShowAll: boolean): void,
}

const initialGetIncomes = () => new Promise<void>(() => {});
const initialIncomesContext: InitialIncomesType = {
  incomes: [],
  getIncomes: initialGetIncomes,
  setIncomesFilterDate: (filterDate: string): void => {},
  isShowAllIncomes: false,
  setIsShowAllIncomes: (isShowAll: boolean) => {},
}

export const IncomesContext = React.createContext<InitialIncomesType>(initialIncomesContext);

type InitialTypesOfSavingType = {
  typesOfSaving: TypeOfSavingCollectionType;
  getTypesOfSaving: (userKey: string) => Promise<void>,
}

const initialGetTypesOfSaving = () => new Promise<void>(() => {});
const initialTypesOfSavingContext: InitialTypesOfSavingType = {
  typesOfSaving: {},
  getTypesOfSaving: initialGetTypesOfSaving,
}

export const TypesOfSavingContext = React.createContext<InitialTypesOfSavingType>(initialTypesOfSavingContext);

function App() {
  const [user, setUser] = useState<UserType>(initialUser);
  const [userInfo, setUserInfo] = useState<UserInfoType>(initialUserInfo);
  const [expenseCategories, setExpenseCategories] = useState<ExpensesCategoryCollectionType>({});
  const [currencyExchange, setCurrencyExchange] = useState<CurrencyExchangeType>(initialCurrencyExchange);
  const [expenses, setExpenses] = useState<ExpenseType[]>([]);
  const [expensesFilterDate, setExpensesFilterDate] = useState<string>('');
  const [isShowAllExpenses, setIsShowAllExpenses] = useState<boolean>(false);
  const [incomes, setIncomes] = useState<IncomeType[]>([]);
  const [incomesFilterDate, setIncomesFilterDate] = useState<string>('');
  const [isShowAllIncomes, setIsShowAllIncomes] = useState<boolean>(false);
  const [typesOfSaving, setTypesOfSaving] = useState<TypeOfSavingCollectionType>({});

  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }

  const auth = firebase.auth();

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      redirectToRelatedPage(Boolean(authUser));

      if (!authUser) {
        setUser({ email: '', key: '' });

        return;
      }

      setUser({ email: auth.currentUser?.email || '', key: auth.currentUser?.uid || '' });
    });
  }, [auth, setUser]);

  const getUserInfo = useCallback(
    async (userKey: string) => {
      const userInfo = await getUserInfoFirestore(userKey);

      if (!userInfo) {
        setUserInfo({ baseCurrencyKey: DEFAULT_BASE_CURRENCY });

        return;
      }

      setUserInfo(userInfo);
    },
    [setUserInfo],
  );

  useEffect(() => {
    if (user.key) {
      getUserInfo(user.key);
    }
  }, [getUserInfo, user]);

  useEffect(() => {
    const getCurrencyExchange = async () => {
      const currencyExchangeList = await getCurrencyExchangeApi();

      setCurrencyExchange(currencyExchangeList);
    }

    getCurrencyExchange();
  }, []);

  const getExpenseCategories = useCallback(
    async (userKey: string): Promise<void> => {
      const expenseCategoriesList = await getExpenseCategoriesFirestore(userKey);

      setExpenseCategories(expenseCategoriesList);
    },
    [setExpenseCategories],
  );

  useEffect(() => {
    if (user.key) {
      getExpenseCategories(user.key);
    }
  }, [getExpenseCategories, user]);

  const getExpenses = useCallback(
    async (userKey: string): Promise<void> => {
      const expensesList = await getExpensesFirestore(expensesFilterDate, isShowAllExpenses, userKey);

      setExpenses(expensesList);
    },
    [setExpenses, expensesFilterDate, isShowAllExpenses],
  );

  useEffect(() => {
    if (user.key) {
      getExpenses(user.key);
    }
  }, [getExpenses, user]);

  const getIncomes = useCallback(
    async (userKey: string): Promise<void> => {
      const incomesList = await getIncomesFirestore(incomesFilterDate, isShowAllIncomes, userKey);

      setIncomes(incomesList);
    },
    [setIncomes, incomesFilterDate, isShowAllIncomes],
  );

  useEffect(() => {
    if (user.key) {
      getIncomes(user.key);
    }
  }, [getIncomes, user]);

  const getTypesOfSaving = useCallback(
    async (userKey: string): Promise<void> => {
      const typesOfSavingList = await getTypesOfSavingFirestore(userKey);

      setTypesOfSaving(typesOfSavingList);
    },
    [setTypesOfSaving],
  );

  useEffect(() => {
    if (user.key) {
      getTypesOfSaving(user.key);
    }
  }, [getTypesOfSaving, user]);

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user }}>
        <UserInfoContext.Provider value={{ userInfo, getUserInfo }}>
          <CurrencyExchangeContext.Provider value={currencyExchange}>
            <ExpenseCategoriesContext.Provider value={{ expenseCategories, getExpenseCategories }}>
              <ExpensesContext.Provider
                value={{
                  expenses,
                  getExpenses,
                  setExpensesFilterDate,
                  isShowAllExpenses,
                  setIsShowAllExpenses,
                }}
              >
                <IncomesContext.Provider
                  value={{
                    incomes,
                    getIncomes,
                    setIncomesFilterDate,
                    isShowAllIncomes,
                    setIsShowAllIncomes,
                  }}
                >
                  <TypesOfSavingContext.Provider value={{ typesOfSaving, getTypesOfSaving }}>
                    <Layout />
                  </TypesOfSavingContext.Provider>
                </IncomesContext.Provider>
              </ExpensesContext.Provider>
            </ExpenseCategoriesContext.Provider>
          </CurrencyExchangeContext.Provider>
        </UserInfoContext.Provider>
      </UserContext.Provider>
    </BrowserRouter>
  );
}

export default App;
