import {UserInfoType} from '../../shared/types/userInfo.type';

export type BaseCurrencyType = Pick<UserInfoType, 'baseCurrencyKey'>;
