import React, {ReactElement, useContext} from 'react';
import {UserContext, UserInfoContext} from '../../App';
import {useForm} from 'react-hook-form';
import {BaseCurrencyType} from '../types/BaseCurrency.type';
import firebase from 'firebase';
import {EntityPathEnum} from '../../shared/enums/entityPath.enum';
import CurrencySelect from '../../shared/components/CurrencySelect';

const Currencies = (): ReactElement => {
  const { userInfo: { baseCurrencyKey }, getUserInfo } = useContext(UserInfoContext);
  const { register, handleSubmit, reset, setValue } = useForm<BaseCurrencyType>();
  const db = firebase.firestore();
  const { user: { key } } = useContext(UserContext);

  const onSubmit = async (value: BaseCurrencyType): Promise<void> => {
    await db.collection(EntityPathEnum.users)
      .doc(key)
      .update(value);

    reset();

    if (key) {
      getUserInfo(key);
    }
  };

  const setSelectCurrency = (currencyKey: string) => setValue('baseCurrencyKey', currencyKey);

  return (
    <form onSubmit={handleSubmit(onSubmit)} autoComplete="off">
      {baseCurrencyKey}
      <CurrencySelect
        {...register('baseCurrencyKey')}
        setInitialCurrencyKey={setSelectCurrency}
      />
      <button type="submit">Save</button>
    </form>
  );
};

export default Currencies;
